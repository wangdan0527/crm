import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';
let RestService = class RestService {
    constructor(http) {
        this.http = http;
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    registerAdmin(name, email, password) {
        let formData = new FormData();
        formData.append('name', name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('type', "1");
        return this.http.post(environment.endpoint + 'account/register', formData).pipe(map(this.extractData));
    }
    registerUser(name, email, password, company_id) {
        console.log(company_id);
        let formData = new FormData();
        formData.append('name', name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('type', "0");
        formData.append('company_id', company_id);
        return this.http.post(environment.endpoint + 'account/register', formData).pipe(map(this.extractData));
    }
    loginAdmin(email, password) {
        let formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);
        return this.http.post(environment.endpoint + 'account/login', formData).pipe(map(this.extractData));
    }
    registerCompany(nameEn, nameAr, logoUrl) {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        let formData = new FormData();
        formData.append('name_en', nameEn);
        formData.append('name_ar', nameAr);
        formData.append('logo_url', logoUrl);
        return this.http.post(environment.endpoint + 'company/register', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getCompanies() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'company/list', { headers: headers }).pipe(map(this.extractData));
    }
    getUsers() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'admin/users', { headers: headers }).pipe(map(this.extractData));
    }
    approveUser(user_id) {
        let formData = new FormData();
        formData.append('id', user_id);
        return this.http.post(environment.endpoint + 'admin/user/approve', formData).pipe(map(this.extractData));
    }
    declineUser(user_id) {
        let formData = new FormData();
        formData.append('id', user_id);
        return this.http.post(environment.endpoint + 'admin/user/decline', formData).pipe(map(this.extractData));
    }
    addProvince(name_en, name_ar, country_id) {
        let formData = new FormData();
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('country_id', country_id);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'province', formData, { headers: headers }).pipe(map(this.extractData));
    }
    addCountry(name_en, name_ar) {
        let formData = new FormData();
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'country', formData, { headers: headers }).pipe(map(this.extractData));
    }
    addCity(name_en, name_ar, province) {
        let formData = new FormData();
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('province_id', province + "");
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'city', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getProvinces() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'province/list', { headers: headers }).pipe(map(this.extractData));
    }
    getCountries() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'country/list', { headers: headers }).pipe(map(this.extractData));
    }
    addVendorType(name) {
        let formData = new FormData();
        formData.append('name', name);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'vendor_type', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getVendorTypes() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'vendor_type/list', { headers: headers }).pipe(map(this.extractData));
    }
    addComplaintType(name) {
        let formData = new FormData();
        formData.append('name', name);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'complaint_type', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getComplaintTypes() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'complaint_type/list', { headers: headers }).pipe(map(this.extractData));
    }
    addCustomerType(name) {
        let formData = new FormData();
        formData.append('name', name);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'customer_type', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getCustomerTypes() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'customer_type/list', { headers: headers }).pipe(map(this.extractData));
    }
    addCustomer(name_en, name_ar, type, phone, mobile, fax, web, country, province, city, address, mobile1, mobile2, email, co_representative, company_email, ipan_no, bank_acc, bank, branch, customer_classification, first_relation, last_relation, remarks) {
        let formData = new FormData();
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('type', type);
        formData.append('phone', phone);
        formData.append('mobile', mobile);
        formData.append('fax', fax);
        formData.append('web', web);
        formData.append('country', country);
        formData.append('province', province);
        formData.append('city', city);
        formData.append('address', address);
        formData.append('mobile1', mobile1);
        formData.append('mobile2', mobile2);
        formData.append('email', email);
        formData.append('co_representative', co_representative);
        formData.append('company_email', company_email);
        formData.append('ipan_no', ipan_no);
        formData.append('bank_acc', bank_acc);
        formData.append('bank', bank);
        formData.append('branch', branch);
        formData.append('customer_classification', customer_classification);
        formData.append('first_relation', first_relation);
        formData.append('last_relation', last_relation);
        formData.append('remarks', remarks);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'customer', formData, { headers: headers }).pipe(map(this.extractData));
    }
    editCustomer(id, name_en, name_ar, type, phone, mobile, fax, web, country, province, city, address, mobile1, mobile2, email, co_representative, company_email, ipan_no, bank_acc, bank, branch, customer_classification, first_relation, last_relation, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('type', type);
        formData.append('phone', phone);
        formData.append('mobile', mobile);
        formData.append('fax', fax);
        formData.append('web', web);
        formData.append('country', country);
        formData.append('province', province);
        formData.append('city', city);
        formData.append('address', address);
        formData.append('mobile1', mobile1);
        formData.append('mobile2', mobile2);
        formData.append('email', email);
        formData.append('co_representative', co_representative);
        formData.append('company_email', company_email);
        formData.append('ipan_no', ipan_no);
        formData.append('bank_acc', bank_acc);
        formData.append('bank', bank);
        formData.append('branch', branch);
        formData.append('customer_classification', customer_classification);
        formData.append('first_relation', first_relation);
        formData.append('last_relation', last_relation);
        formData.append('remarks', remarks);
        console.log(formData);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'customer/update', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getCustomers() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'customer/list', { headers: headers }).pipe(map(this.extractData));
    }
    addVendor(name_en, name_ar, type, phone, mobile, fax, web, country, province, city, address, mobile1, mobile2, email, co_representative, company_email, ipan_no, bank_acc, bank, branch, vendor_classification, first_relation, last_relation, remarks) {
        let formData = new FormData();
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('type', type);
        formData.append('phone', phone);
        formData.append('mobile', mobile);
        formData.append('fax', fax);
        formData.append('web', web);
        formData.append('country', country);
        formData.append('province', province);
        formData.append('city', city);
        formData.append('address', address);
        formData.append('mobile1', mobile1);
        formData.append('mobile2', mobile2);
        formData.append('email', email);
        formData.append('co_representative', co_representative);
        formData.append('company_email', company_email);
        formData.append('ipan_no', ipan_no);
        formData.append('bank_acc', bank_acc);
        formData.append('bank', bank);
        formData.append('branch', branch);
        formData.append('vendor_classification', vendor_classification);
        formData.append('first_relation', first_relation);
        formData.append('last_relation', last_relation);
        formData.append('remarks', remarks);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'vendor', formData, { headers: headers }).pipe(map(this.extractData));
    }
    editVendor(id, name_en, name_ar, type, phone, mobile, fax, web, country, province, city, address, mobile1, mobile2, email, co_representative, company_email, ipan_no, bank_acc, bank, branch, vendor_classification, first_relation, last_relation, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('name_en', name_en);
        formData.append('name_ar', name_ar);
        formData.append('type', type);
        formData.append('phone', phone);
        formData.append('mobile', mobile);
        formData.append('fax', fax);
        formData.append('web', web);
        formData.append('country', country);
        formData.append('province', province);
        formData.append('city', city);
        formData.append('address', address);
        formData.append('mobile1', mobile1);
        formData.append('mobile2', mobile2);
        formData.append('email', email);
        formData.append('co_representative', co_representative);
        formData.append('company_email', company_email);
        formData.append('ipan_no', ipan_no);
        formData.append('bank_acc', bank_acc);
        formData.append('bank', bank);
        formData.append('branch', branch);
        formData.append('vendor_classification', vendor_classification);
        formData.append('first_relation', first_relation);
        formData.append('last_relation', last_relation);
        formData.append('remarks', remarks);
        console.log(formData);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'vendor/update', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getVendors() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'vendor/list', { headers: headers }).pipe(map(this.extractData));
    }
    addDepartment(name, company_id) {
        let formData = new FormData();
        formData.append('name', name);
        formData.append('company_id', company_id);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'department', formData, { headers: headers }).pipe(map(this.extractData));
    }
    deleteDepartment(id) {
        let formData = new FormData();
        formData.append('id', id);
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.post(environment.endpoint + 'department/delete', formData, { headers: headers }).pipe(map(this.extractData));
    }
    getDepartments() {
        const headers = new HttpHeaders()
            .set("token", localStorage.getItem("token"));
        return this.http.get(environment.endpoint + 'department/list', { headers: headers }).pipe(map(this.extractData));
    }
};
RestService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [HttpClient])
], RestService);
export { RestService };
//# sourceMappingURL=rest-client.service.js.map