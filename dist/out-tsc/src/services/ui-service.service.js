import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PopUpComponent } from '../app/ui/pop-up/pop-up.component';
import { BehaviorSubject } from 'rxjs';
let UiServiceService = class UiServiceService {
    constructor(dialog) {
        this.dialog = dialog;
    }
    openDeleteConfirmDialog() {
        var subject = new BehaviorSubject(false);
        const dialogRef = this.dialog.open(PopUpComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == 1) {
                subject.next(true);
            }
        });
        return subject;
    }
};
UiServiceService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [MatDialog])
], UiServiceService);
export { UiServiceService };
//# sourceMappingURL=ui-service.service.js.map