import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
let PopUpComponent = class PopUpComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
    onClose() {
        this.dialogRef.close(0);
    }
    onDelete() {
        this.dialogRef.close(1);
    }
};
PopUpComponent = __decorate([
    Component({
        selector: 'app-pop-up',
        templateUrl: './pop-up.component.html',
        styleUrls: ['./pop-up.component.scss']
    }),
    __metadata("design:paramtypes", [MatDialogRef])
], PopUpComponent);
export { PopUpComponent };
//# sourceMappingURL=pop-up.component.js.map