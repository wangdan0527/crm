import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProvinceSetupComponent } from './province-setup/province-setup.component';
const routes = [
    {
        path: '', component: ProvinceSetupComponent
    }
];
let ProvinceSetupRoutingModule = class ProvinceSetupRoutingModule {
};
ProvinceSetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ProvinceSetupRoutingModule);
export { ProvinceSetupRoutingModule };
//# sourceMappingURL=province-setup-routing.module.js.map