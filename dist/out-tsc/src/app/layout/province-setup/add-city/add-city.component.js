import { __decorate, __metadata, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
let AddCityComponent = class AddCityComponent {
    constructor(restService, dialogRef, data) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.name_ar = "";
        this.name_en = "";
        this.province = -1;
        this.provinces = [];
    }
    ngOnInit() {
    }
    onSaveCity() {
        if (!this.name_ar) {
            return;
        }
        if (!this.name_en) {
            return;
        }
        this.restService.addCity(this.name_en, this.name_ar, this.data.province_id).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
AddCityComponent = __decorate([
    Component({
        selector: 'app-add-city',
        templateUrl: './add-city.component.html',
        styleUrls: ['./add-city.component.scss']
    }),
    __param(2, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef, Object])
], AddCityComponent);
export { AddCityComponent };
//# sourceMappingURL=add-city.component.js.map