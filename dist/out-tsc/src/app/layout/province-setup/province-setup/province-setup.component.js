import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service';
import { AddProvinceComponent } from '../add-province/add-province.component';
import { AddCityComponent } from '../add-city/add-city.component';
import { AddCountryComponent } from '../add-country/add-country.component';
let ProvinceSetupComponent = class ProvinceSetupComponent {
    constructor(restService, dialog) {
        this.restService = restService;
        this.dialog = dialog;
        this.provinces = [];
        this.cities = [];
        this.countries = [];
    }
    ngOnInit() {
        this.loadMap();
    }
    loadMap() {
        this.restService.getProvinces().subscribe(response => {
            console.log(response);
            this.provinces = response.province;
            this.cities = response.city;
            this.countries = response.country;
            for (var i = 0; i < this.cities.length; i++) {
                for (var j = 0; j < this.provinces.length; j++) {
                    if (this.cities[i].province_id == this.provinces[j].id) {
                        if (this.provinces[j].cities == undefined) {
                            this.provinces[j].cities = [this.cities[i]];
                        }
                        else {
                            this.provinces[j].cities.push(this.cities[i]);
                        }
                    }
                }
            }
            for (var i = 0; i < this.provinces.length; i++) {
                for (var j = 0; j < this.countries.length; j++) {
                    if (this.provinces[i].country_id == this.countries[j].id) {
                        if (this.countries[j].provinces == undefined) {
                            this.countries[j].provinces = [this.provinces[i]];
                        }
                        else {
                            this.countries[j].provinces.push(this.provinces[i]);
                        }
                    }
                }
            }
            console.log(this.countries);
        });
    }
    onAddProvince(country_id) {
        console.log(country_id);
        const dialogRef = this.dialog.open(AddProvinceComponent, {
            width: '500px',
            data: {
                country_id: country_id
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadMap();
        });
    }
    onAddCountry() {
        const dialogRef = this.dialog.open(AddCountryComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    onAddCity(province_id) {
        const dialogRef = this.dialog.open(AddCityComponent, {
            width: '500px',
            data: {
                province_id: province_id
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadMap();
        });
    }
};
ProvinceSetupComponent = __decorate([
    Component({
        selector: 'app-province-setup',
        templateUrl: './province-setup.component.html',
        styleUrls: ['./province-setup.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialog])
], ProvinceSetupComponent);
export { ProvinceSetupComponent };
//# sourceMappingURL=province-setup.component.js.map