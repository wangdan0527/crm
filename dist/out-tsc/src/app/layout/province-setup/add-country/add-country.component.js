import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let AddCountryComponent = class AddCountryComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.name_ar = "";
        this.name_en = "";
    }
    ngOnInit() {
    }
    onSaveProvince() {
        if (!this.name_ar) {
            return;
        }
        if (!this.name_en) {
            return;
        }
        this.restService.addCountry(this.name_en, this.name_ar).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
AddCountryComponent = __decorate([
    Component({
        selector: 'app-add-country',
        templateUrl: './add-country.component.html',
        styleUrls: ['./add-country.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], AddCountryComponent);
export { AddCountryComponent };
//# sourceMappingURL=add-country.component.js.map