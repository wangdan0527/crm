import { __decorate, __metadata, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
let AddProvinceComponent = class AddProvinceComponent {
    constructor(restService, dialogRef, data) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.name_ar = "";
        this.name_en = "";
    }
    ngOnInit() {
        console.log(this.data.country_id);
    }
    onSaveProvince() {
        if (!this.name_ar) {
            return;
        }
        if (!this.name_en) {
            return;
        }
        this.restService.addProvince(this.name_en, this.name_ar, this.data.country_id).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
AddProvinceComponent = __decorate([
    Component({
        selector: 'app-add-province',
        templateUrl: './add-province.component.html',
        styleUrls: ['./add-province.component.scss']
    }),
    __param(2, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef, Object])
], AddProvinceComponent);
export { AddProvinceComponent };
//# sourceMappingURL=add-province.component.js.map