import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../../../services/ui-service.service';
let HeaderComponent = class HeaderComponent {
    constructor(translate, router, uiSerivce) {
        this.translate = translate;
        this.router = router;
        this.uiSerivce = uiSerivce;
        this.userName = "";
        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }
    ngOnInit() {
        this.pushRightClass = 'push-right';
        this.userName = localStorage.getItem("name");
    }
    isToggled() {
        const dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }
    toggleSidebar() {
        const dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }
    rltAndLtr() {
        const dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    }
    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }
    changeLang(language) {
        const dom = document.querySelector('body');
        this.translate.use(language);
        if (language == "ar") {
            dom.classList.add('rtl');
            // this.uiSerivce.orientationChange.emit("rtl")
        }
        else {
            dom.classList.remove('rtl');
            // this.uiSerivce.orientationChange.next("")
        }
    }
};
HeaderComponent = __decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.scss']
    }),
    __metadata("design:paramtypes", [TranslateService,
        Router,
        UiServiceService])
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map