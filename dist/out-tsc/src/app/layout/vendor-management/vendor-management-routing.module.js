import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VendorManagementComponent } from './vendor-management/vendor-management.component';
const routes = [
    {
        path: '', component: VendorManagementComponent
    }
];
let VendorManagementRoutingModule = class VendorManagementRoutingModule {
};
VendorManagementRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], VendorManagementRoutingModule);
export { VendorManagementRoutingModule };
//# sourceMappingURL=vendor-management-routing.module.js.map