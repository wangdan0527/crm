import { __decorate, __metadata, __param } from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
let VendorDetailsComponent = class VendorDetailsComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.first_relation = "";
        this.last_relation = "";
    }
    ngOnInit() {
        console.log(this.data);
        this.vendor = this.data;
        let dateStart = new Date(this.vendor.first_relation * 1000);
        this.first_relation = dateStart.toLocaleString();
        let dateLast = new Date(this.vendor.last_relation * 1000);
        this.last_relation = dateLast.toLocaleString();
    }
};
VendorDetailsComponent = __decorate([
    Component({
        selector: 'app-vendor-details',
        templateUrl: './vendor-details.component.html',
        styleUrls: ['./vendor-details.component.scss']
    }),
    __param(1, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [MatDialogRef, Object])
], VendorDetailsComponent);
export { VendorDetailsComponent };
//# sourceMappingURL=vendor-details.component.js.map