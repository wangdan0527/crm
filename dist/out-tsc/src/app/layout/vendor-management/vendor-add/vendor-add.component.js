import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let VendorAddComponent = class VendorAddComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.types = [];
        this.provinces = [];
        this.countries = [];
        this.cities = [];
        this.name_ar = "";
        this.type = -1;
        this.name_en = "";
        this.phone = "";
        this.fax = "";
        this.mobile = "";
        this.web = "";
        this.country = -1;
        this.province = -1;
        this.city = -1;
        this.address = "";
        this.mobile1 = "";
        this.mobile2 = "";
        this.company_email = "";
        this.co_representative = "";
        this.email = "";
        this.ipan_no = "";
        this.bank_acc = "";
        this.bank = "";
        this.branch = "";
        this.vendor_classification = "";
        this.first_relation = null;
        this.last_relation = null;
        this.remarks = "";
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        this.restService.getCountries().subscribe(response => {
            this.countries = response.countries;
        });
        this.restService.getProvinces().subscribe(response => {
            this.provinces = response.province;
            this.cities = response.city;
            console.log(this.cities);
        });
        this.restService.getVendorTypes().subscribe(response => {
            console.log(response);
            this.types = response.vendorTypes;
        });
    }
    onSaveVendor() {
        this.restService.addVendor(this.name_en, this.name_ar, this.type + "", this.phone, this.mobile, this.fax, this.web, this.country + "", this.province + "", this.city + "", this.address, this.mobile1, this.mobile2, this.email, this.co_representative, this.company_email, this.ipan_no, this.bank_acc, this.bank, this.branch, this.vendor_classification, this.first_relation.getTime() / 1000 + "", this.last_relation.getTime() / 1000 + "", this.remarks).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
VendorAddComponent = __decorate([
    Component({
        selector: 'app-vendor-add',
        templateUrl: './vendor-add.component.html',
        styleUrls: ['./vendor-add.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], VendorAddComponent);
export { VendorAddComponent };
//# sourceMappingURL=vendor-add.component.js.map