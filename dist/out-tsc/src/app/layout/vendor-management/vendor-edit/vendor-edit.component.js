import { __decorate, __metadata, __param } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
let VendorEditComponent = class VendorEditComponent {
    constructor(restService, dialogRef, data) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.types = [];
        this.provinces = [];
        this.countries = [];
        this.cities = [];
        this.name_ar = "";
        this.type = -1;
        this.name_en = "";
        this.phone = "";
        this.fax = "";
        this.mobile = "";
        this.web = "";
        this.country = -1;
        this.province = -1;
        this.city = -1;
        this.address = "";
        this.mobile1 = "";
        this.mobile2 = "";
        this.company_email = "";
        this.co_representative = "";
        this.email = "";
        this.ipan_no = "";
        this.bank_acc = "";
        this.bank = "";
        this.branch = "";
        this.vendor_classification = "";
        this.first_relation = null;
        this.last_relation = null;
        this.remarks = "";
    }
    ngOnInit() {
        this.loadData();
        this.vendor = this.data;
        this.name_en = this.vendor.name_en;
        this.name_ar = this.vendor.name_ar;
        this.type = this.vendor.type;
        this.phone = this.vendor.phone;
        this.mobile = this.vendor.mobile;
        this.fax = this.vendor.fax;
        this.web = this.vendor.web;
        this.country = this.vendor.country;
        this.province = this.vendor.province;
        this.city = this.vendor.city;
        this.address = this.vendor.address;
        this.mobile1 = this.vendor.mobile1;
        this.mobile2 = this.vendor.mobile2;
        this.email = this.vendor.email;
        this.co_representative = this.vendor.co_representative;
        this.company_email = this.vendor.company_email;
        this.ipan_no = this.vendor.ipan_no;
        this.bank_acc = this.vendor.bank_acc;
        this.bank = this.vendor.bank;
        this.branch = this.vendor.branch;
        this.vendor_classification = this.vendor.vendor_classification;
        this.first_relation = new Date(this.vendor.first_relation * 1000);
        this.last_relation = new Date(this.vendor.last_relation * 1000);
        this.remarks = this.vendor.remarks;
    }
    loadData() {
        this.restService.getCountries().subscribe(response => {
            this.countries = response.countries;
        });
        this.restService.getProvinces().subscribe(response => {
            this.provinces = response.province;
            this.cities = response.city;
        });
        this.restService.getVendorTypes().subscribe(response => {
            this.types = response.vendorTypes;
        });
    }
    onSaveVendor() {
        this.restService.editVendor(this.vendor.id, this.name_en, this.name_ar, this.type + "", this.phone, this.mobile, this.fax, this.web, this.country + "", this.province + "", this.city + "", this.address, this.mobile1, this.mobile2, this.email, this.co_representative, this.company_email, this.ipan_no, this.bank_acc, this.bank, this.branch, this.vendor_classification, this.first_relation.getTime() / 1000 + "", this.last_relation.getTime() / 1000 + "", this.remarks).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
VendorEditComponent = __decorate([
    Component({
        selector: 'app-vendor-edit',
        templateUrl: './vendor-edit.component.html',
        styleUrls: ['./vendor-edit.component.scss']
    }),
    __param(2, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef, Object])
], VendorEditComponent);
export { VendorEditComponent };
//# sourceMappingURL=vendor-edit.component.js.map