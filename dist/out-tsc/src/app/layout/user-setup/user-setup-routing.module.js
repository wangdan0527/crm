import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserSetupComponent } from './user-setup/user-setup.component';
const routes = [
    {
        path: '', component: UserSetupComponent
    }
];
let UserSetupRoutingModule = class UserSetupRoutingModule {
};
UserSetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], UserSetupRoutingModule);
export { UserSetupRoutingModule };
//# sourceMappingURL=user-setup-routing.module.js.map