import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UiServiceService } from '../../../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
let UserSetupComponent = class UserSetupComponent {
    constructor(uiSerivce, restService, translate) {
        this.uiSerivce = uiSerivce;
        this.restService = restService;
        this.translate = translate;
        this.displayedColumns = ['id', 'name', 'email', 'company_name_e', 'approved', 'menu'];
        this.dataSource = new MatTableDataSource([]);
        this.users = [];
        this.orientation = "";
    }
    ngOnInit() {
        this.loadUsers();
        this.dataSource.sort = this.sort;
        if (this.translate.currentLang == "ar") {
            this.orientation = "rtl";
        }
        // this.uiSerivce.orientationChange.subscribe(orientation => {
        //   this.orientation = orientation;
        // })
    }
    loadUsers() {
        this.restService.getUsers().subscribe(response => {
            this.dataSource = new MatTableDataSource(response.result);
            this.dataSource.sort = this.sort;
            this.users = response.result;
        });
    }
    onApprove(element) {
        this.restService.approveUser(element.user_id).subscribe(response => {
        });
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].user_id == element.user_id) {
                this.users[i].approved = 1;
            }
        }
    }
    onDecline(element) {
        this.restService.declineUser(element.user_id).subscribe(response => {
        });
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].user_id == element.user_id) {
                this.users[i].approved = 0;
            }
        }
    }
};
__decorate([
    ViewChild(MatSort, { static: true }),
    __metadata("design:type", MatSort)
], UserSetupComponent.prototype, "sort", void 0);
UserSetupComponent = __decorate([
    Component({
        selector: 'app-user-setup',
        templateUrl: './user-setup.component.html',
        styleUrls: ['./user-setup.component.scss']
    }),
    __metadata("design:paramtypes", [UiServiceService,
        RestService,
        TranslateService])
], UserSetupComponent);
export { UserSetupComponent };
//# sourceMappingURL=user-setup.component.js.map