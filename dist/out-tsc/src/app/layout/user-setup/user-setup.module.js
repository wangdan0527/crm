import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSetupComponent } from './user-setup/user-setup.component';
import { UserSetupRoutingModule } from './user-setup-routing.module';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
let UserSetupModule = class UserSetupModule {
};
UserSetupModule = __decorate([
    NgModule({
        declarations: [UserSetupComponent],
        imports: [
            CommonModule,
            UserSetupRoutingModule,
            MatTableModule,
            MatSortModule,
            MatMenuModule,
            MatIconModule,
            MatButtonModule,
            TranslateModule
        ]
    })
], UserSetupModule);
export { UserSetupModule };
//# sourceMappingURL=user-setup.module.js.map