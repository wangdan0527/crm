import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VendorSetupComponent } from './vendor-setup/vendor-setup.component';
const routes = [
    {
        path: '', component: VendorSetupComponent
    }
];
let VendorSetupRoutingModule = class VendorSetupRoutingModule {
};
VendorSetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], VendorSetupRoutingModule);
export { VendorSetupRoutingModule };
//# sourceMappingURL=vendor-setup-routing.module.js.map