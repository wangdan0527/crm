import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service';
import { AddVendorComponent } from '../add-vendor/add-vendor.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
let VendorSetupComponent = class VendorSetupComponent {
    constructor(restService, dialog) {
        this.restService = restService;
        this.dialog = dialog;
        this.displayedColumns = ['id', 'name'];
        this.dataSource = new MatTableDataSource([]);
        this.users = [];
    }
    ngOnInit() {
        this.loadVendors();
        this.dataSource.sort = this.sort;
    }
    loadVendors() {
        this.restService.getVendorTypes().subscribe(response => {
            console.log(response);
            this.dataSource = new MatTableDataSource(response.vendorTypes);
            this.dataSource.sort = this.sort;
        });
    }
    onAddVendorType() {
        const dialogRef = this.dialog.open(AddVendorComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadVendors();
        });
    }
};
__decorate([
    ViewChild(MatSort, { static: true }),
    __metadata("design:type", MatSort)
], VendorSetupComponent.prototype, "sort", void 0);
VendorSetupComponent = __decorate([
    Component({
        selector: 'app-vendor-setup',
        templateUrl: './vendor-setup.component.html',
        styleUrls: ['./vendor-setup.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialog])
], VendorSetupComponent);
export { VendorSetupComponent };
//# sourceMappingURL=vendor-setup.component.js.map