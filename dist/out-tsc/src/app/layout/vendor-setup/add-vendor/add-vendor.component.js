import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let AddVendorComponent = class AddVendorComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.name = "";
    }
    ngOnInit() {
    }
    onSaveVendor() {
        if (!this.name) {
            return;
        }
        this.restService.addVendorType(this.name).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
AddVendorComponent = __decorate([
    Component({
        selector: 'app-add-vendor',
        templateUrl: './add-vendor.component.html',
        styleUrls: ['./add-vendor.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], AddVendorComponent);
export { AddVendorComponent };
//# sourceMappingURL=add-vendor.component.js.map