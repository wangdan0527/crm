import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomerTypeSetupComponent } from './customer-type-setup/customer-type-setup.component';
const routes = [
    {
        path: '', component: CustomerTypeSetupComponent
    }
];
let CustomerTypeSetupRoutingModule = class CustomerTypeSetupRoutingModule {
};
CustomerTypeSetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CustomerTypeSetupRoutingModule);
export { CustomerTypeSetupRoutingModule };
//# sourceMappingURL=customer-type-setup-routing.module.js.map