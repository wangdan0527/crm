import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let CustomerTypeAddComponent = class CustomerTypeAddComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.name = "";
    }
    ngOnInit() {
    }
    onSaveCustomerType() {
        if (!this.name) {
            return;
        }
        this.restService.addCustomerType(this.name).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
CustomerTypeAddComponent = __decorate([
    Component({
        selector: 'app-customer-type-add',
        templateUrl: './customer-type-add.component.html',
        styleUrls: ['./customer-type-add.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], CustomerTypeAddComponent);
export { CustomerTypeAddComponent };
//# sourceMappingURL=customer-type-add.component.js.map