import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service';
import { CustomerTypeAddComponent } from '../customer-type-add/customer-type-add.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
let CustomerTypeSetupComponent = class CustomerTypeSetupComponent {
    constructor(restService, dialog) {
        this.restService = restService;
        this.dialog = dialog;
        this.displayedColumns = ['id', 'name'];
        this.dataSource = new MatTableDataSource([]);
        this.users = [];
    }
    ngOnInit() {
        this.loadCustomerTypes();
        this.dataSource.sort = this.sort;
    }
    loadCustomerTypes() {
        this.restService.getCustomerTypes().subscribe(response => {
            console.log(response);
            this.dataSource = new MatTableDataSource(response.customerTypes);
            this.dataSource.sort = this.sort;
        });
    }
    onAddCustomerType() {
        const dialogRef = this.dialog.open(CustomerTypeAddComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadCustomerTypes();
        });
    }
};
__decorate([
    ViewChild(MatSort, { static: true }),
    __metadata("design:type", MatSort)
], CustomerTypeSetupComponent.prototype, "sort", void 0);
CustomerTypeSetupComponent = __decorate([
    Component({
        selector: 'app-customer-type-setup',
        templateUrl: './customer-type-setup.component.html',
        styleUrls: ['./customer-type-setup.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialog])
], CustomerTypeSetupComponent);
export { CustomerTypeSetupComponent };
//# sourceMappingURL=customer-type-setup.component.js.map