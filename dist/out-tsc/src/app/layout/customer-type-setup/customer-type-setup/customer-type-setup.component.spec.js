import { async, TestBed } from '@angular/core/testing';
import { CustomerTypeSetupComponent } from './customer-type-setup.component';
describe('CustomerTypeSetupComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomerTypeSetupComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(CustomerTypeSetupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=customer-type-setup.component.spec.js.map