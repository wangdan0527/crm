import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service';
import { UiServiceService } from '../../../../services/ui-service.service';
import { DepartmentAddComponent } from '../department-add/department-add.component';
import { DepartmentEditComponent } from '../department-edit/department-edit.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
let DepartmentManagementComponent = class DepartmentManagementComponent {
    constructor(restService, uiService, dialog) {
        this.restService = restService;
        this.uiService = uiService;
        this.dialog = dialog;
        this.displayedColumns = ['id', 'name', 'company_info', 'menu'];
        this.dataSource = new MatTableDataSource([]);
        this.departments = [];
    }
    ngOnInit() {
        this.loadDepartments();
    }
    loadDepartments() {
        this.restService.getDepartments().subscribe(response => {
            this.setupTable(response);
        });
    }
    setupTable(response) {
        for (var i = 0; i < response.departments.length; i++) {
            response.departments[i].company_info = response.departments[i].company_name_e + " (" + response.departments[i].company_name_a + ")";
        }
        this.dataSource = new MatTableDataSource(response.departments);
        this.dataSource.sort = this.sort;
    }
    onAddDepartment() {
        const dialogRef = this.dialog.open(DepartmentAddComponent, {
            width: '600px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadDepartments();
        });
    }
    onDelete(department, index) {
        this.uiService.openDeleteConfirmDialog().subscribe(response => {
            if (response == true) {
                this.restService.deleteDepartment(department.id).subscribe(result => {
                    this.setupTable(result);
                });
            }
        });
    }
    onEdit(department, index) {
        const dialogRef = this.dialog.open(DepartmentEditComponent, {
            width: '600px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadDepartments();
        });
    }
};
__decorate([
    ViewChild(MatSort, { static: true }),
    __metadata("design:type", MatSort)
], DepartmentManagementComponent.prototype, "sort", void 0);
DepartmentManagementComponent = __decorate([
    Component({
        selector: 'app-department-management',
        templateUrl: './department-management.component.html',
        styleUrls: ['./department-management.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        UiServiceService,
        MatDialog])
], DepartmentManagementComponent);
export { DepartmentManagementComponent };
//# sourceMappingURL=department-management.component.js.map