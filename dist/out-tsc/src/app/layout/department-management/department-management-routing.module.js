import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DepartmentManagementComponent } from './department-management/department-management.component';
const routes = [
    {
        path: '', component: DepartmentManagementComponent
    }
];
let DepartmentManagementRoutingModule = class DepartmentManagementRoutingModule {
};
DepartmentManagementRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], DepartmentManagementRoutingModule);
export { DepartmentManagementRoutingModule };
//# sourceMappingURL=department-management-routing.module.js.map