import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let DepartmentAddComponent = class DepartmentAddComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.companies = [];
        this.name = "";
        this.company = -1;
        this.errorMsg = "";
    }
    ngOnInit() {
        this.loadCompanies();
    }
    loadCompanies() {
        this.restService.getCompanies().subscribe(response => {
            this.companies = response.result;
            console.log(this.companies);
        });
    }
    onSaveDepartment() {
        if (this.company == -1) {
            this.errorMsg = "Please select company.";
            return;
        }
        if (!this.name) {
            this.errorMsg = "Please add name.";
            return;
        }
        this.restService.addDepartment(this.name, this.company + "").subscribe(response => {
            this.dialogRef.close();
        });
    }
};
DepartmentAddComponent = __decorate([
    Component({
        selector: 'app-department-add',
        templateUrl: './department-add.component.html',
        styleUrls: ['./department-add.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], DepartmentAddComponent);
export { DepartmentAddComponent };
//# sourceMappingURL=department-add.component.js.map