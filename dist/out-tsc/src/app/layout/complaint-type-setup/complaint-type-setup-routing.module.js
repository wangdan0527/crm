import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComplaintTypeSetupComponent } from './complaint-type-setup/complaint-type-setup.component';
const routes = [
    {
        path: '', component: ComplaintTypeSetupComponent
    }
];
let ComplaintTypeSetupRoutingModule = class ComplaintTypeSetupRoutingModule {
};
ComplaintTypeSetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ComplaintTypeSetupRoutingModule);
export { ComplaintTypeSetupRoutingModule };
//# sourceMappingURL=complaint-type-setup-routing.module.js.map