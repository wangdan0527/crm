import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service';
import { ComplaintTypeAddComponent } from '../complaint-type-add/complaint-type-add.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
let ComplaintTypeSetupComponent = class ComplaintTypeSetupComponent {
    constructor(restService, dialog) {
        this.restService = restService;
        this.dialog = dialog;
        this.displayedColumns = ['id', 'name'];
        this.dataSource = new MatTableDataSource([]);
        this.users = [];
    }
    ngOnInit() {
        this.loadComplaintTypes();
        this.dataSource.sort = this.sort;
    }
    loadComplaintTypes() {
        this.restService.getComplaintTypes().subscribe(response => {
            this.dataSource = new MatTableDataSource(response.vendorTypes);
            this.dataSource.sort = this.sort;
        });
    }
    onAddVendorType() {
        const dialogRef = this.dialog.open(ComplaintTypeAddComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadComplaintTypes();
        });
    }
};
__decorate([
    ViewChild(MatSort, { static: true }),
    __metadata("design:type", MatSort)
], ComplaintTypeSetupComponent.prototype, "sort", void 0);
ComplaintTypeSetupComponent = __decorate([
    Component({
        selector: 'app-complaint-type-setup',
        templateUrl: './complaint-type-setup.component.html',
        styleUrls: ['./complaint-type-setup.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialog])
], ComplaintTypeSetupComponent);
export { ComplaintTypeSetupComponent };
//# sourceMappingURL=complaint-type-setup.component.js.map