import { async, TestBed } from '@angular/core/testing';
import { ComplaintTypeSetupComponent } from './complaint-type-setup.component';
describe('ComplaintTypeSetupComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ComplaintTypeSetupComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ComplaintTypeSetupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=complaint-type-setup.component.spec.js.map