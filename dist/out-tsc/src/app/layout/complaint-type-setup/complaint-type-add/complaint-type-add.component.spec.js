import { async, TestBed } from '@angular/core/testing';
import { ComplaintTypeAddComponent } from './complaint-type-add.component';
describe('ComplaintTypeAddComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ComplaintTypeAddComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ComplaintTypeAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=complaint-type-add.component.spec.js.map