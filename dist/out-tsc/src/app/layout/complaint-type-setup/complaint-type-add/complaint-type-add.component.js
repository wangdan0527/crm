import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef } from '@angular/material/dialog';
let ComplaintTypeAddComponent = class ComplaintTypeAddComponent {
    constructor(restService, dialogRef) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.name = "";
    }
    ngOnInit() {
    }
    onSaveComplaintType() {
        if (!this.name) {
            return;
        }
        this.restService.addComplaintType(this.name).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
ComplaintTypeAddComponent = __decorate([
    Component({
        selector: 'app-complaint-type-add',
        templateUrl: './complaint-type-add.component.html',
        styleUrls: ['./complaint-type-add.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef])
], ComplaintTypeAddComponent);
export { ComplaintTypeAddComponent };
//# sourceMappingURL=complaint-type-add.component.js.map