import { __decorate, __metadata } from "tslib";
import { Component, NgZone } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RestService } from '../../../services/rest-client.service';
let FormComponent = class FormComponent {
    constructor(service, ngZone) {
        this.ngZone = ngZone;
        this.companyName = "";
        this.departmentName = "";
        this.projectName = "";
        this.selectedCompany = "";
        this.selectedDepartmentCompany = "";
        this.selectedDepartment = "";
        this.companies = [];
        this.departments = [];
        this.service = service;
    }
    ngOnInit() {
    }
};
FormComponent = __decorate([
    Component({
        selector: 'app-form',
        templateUrl: './form.component.html',
        styleUrls: ['./form.component.scss'],
        animations: [routerTransition()]
    }),
    __metadata("design:paramtypes", [RestService, NgZone])
], FormComponent);
export { FormComponent };
//# sourceMappingURL=form.component.js.map