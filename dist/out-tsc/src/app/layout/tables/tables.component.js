import { __decorate, __metadata } from "tslib";
import { Component, NgZone } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RestService } from '../../../services/rest-client.service';
let TablesComponent = class TablesComponent {
    constructor(service, ngZone) {
        this.ngZone = ngZone;
        this.companies = [];
        this.departments = [];
        this.projects = [];
        this.selectedCompany = "";
        this.selectedDepartment = "";
        this.projectStatus = "";
        this.nextStage = "";
        this.comment = "";
        this.comments = [];
        this.service = service;
    }
    ngOnInit() {
    }
};
TablesComponent = __decorate([
    Component({
        selector: 'app-tables',
        templateUrl: './tables.component.html',
        styleUrls: ['./tables.component.scss'],
        animations: [routerTransition()]
    }),
    __metadata("design:paramtypes", [RestService, NgZone])
], TablesComponent);
export { TablesComponent };
//# sourceMappingURL=tables.component.js.map