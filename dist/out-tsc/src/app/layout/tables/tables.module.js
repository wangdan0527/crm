import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablesRoutingModule } from './tables-routing.module';
import { TablesComponent } from './tables.component';
import { PageHeaderModule } from './../../shared';
import { FormsModule } from '@angular/forms';
let TablesModule = class TablesModule {
};
TablesModule = __decorate([
    NgModule({
        imports: [CommonModule, TablesRoutingModule, PageHeaderModule, FormsModule],
        declarations: [TablesComponent]
    })
], TablesModule);
export { TablesModule };
//# sourceMappingURL=tables.module.js.map