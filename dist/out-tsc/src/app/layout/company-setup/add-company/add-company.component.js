import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { UtilsService } from '../../../../environments/utils.service';
import { MatDialogRef } from '@angular/material/dialog';
let AddCompanyComponent = class AddCompanyComponent {
    constructor(restService, storage, dialogRef) {
        this.restService = restService;
        this.storage = storage;
        this.dialogRef = dialogRef;
        this.name_ar = "";
        this.name_en = "";
        this.uploadPercent = 0;
    }
    ngOnInit() {
    }
    onSaveCompany() {
        if (!this.file) {
            return;
        }
        if (!this.name_ar) {
            return;
        }
        if (!this.name_en) {
            return;
        }
        const filePath = UtilsService.calculateArea();
        const fileRef = this.storage.ref(filePath);
        const task = this.storage.upload(filePath, this.file);
        // observe percentage changes
        task.percentageChanges().subscribe(progress => {
            console.log(progress);
        });
        // get notified when the download URL is available
        task.snapshotChanges().pipe(finalize(() => fileRef.getDownloadURL().subscribe(result => {
            this.downloadURL = result;
            this.registerCompany();
        })))
            .subscribe();
    }
    registerCompany() {
        this.restService.registerCompany(this.name_en, this.name_ar, this.downloadURL)
            .subscribe(response => {
            this.dialogRef.close();
        });
    }
    uploadFile(event) {
        this.file = event.target.files[0];
    }
};
AddCompanyComponent = __decorate([
    Component({
        selector: 'app-add-company',
        templateUrl: './add-company.component.html',
        styleUrls: ['./add-company.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        AngularFireStorage,
        MatDialogRef])
], AddCompanyComponent);
export { AddCompanyComponent };
//# sourceMappingURL=add-company.component.js.map