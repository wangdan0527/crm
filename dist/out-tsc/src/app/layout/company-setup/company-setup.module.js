import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanySetupComponent } from './company-setup/company-setup.component';
import { CompanySetupRoutingModule } from './company-setup-routing.module';
import { AddCompanyComponent } from './add-company/add-company.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
let CompanySetupModule = class CompanySetupModule {
};
CompanySetupModule = __decorate([
    NgModule({
        declarations: [CompanySetupComponent, AddCompanyComponent],
        imports: [
            CommonModule,
            CompanySetupRoutingModule,
            MatDialogModule,
            MatButtonModule,
            MatFormFieldModule,
            MatInputModule,
            MatTableModule,
            TranslateModule,
            FormsModule
        ],
        entryComponents: [
            AddCompanyComponent,
        ]
    })
], CompanySetupModule);
export { CompanySetupModule };
//# sourceMappingURL=company-setup.module.js.map