import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CompanySetupComponent } from './company-setup/company-setup.component';
const routes = [
    {
        path: '', component: CompanySetupComponent
    }
];
let CompanySetupRoutingModule = class CompanySetupRoutingModule {
};
CompanySetupRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CompanySetupRoutingModule);
export { CompanySetupRoutingModule };
//# sourceMappingURL=company-setup-routing.module.js.map