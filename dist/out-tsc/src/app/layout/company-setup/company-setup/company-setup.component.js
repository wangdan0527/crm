import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddCompanyComponent } from '../add-company/add-company.component';
import { RestService } from '../../../../services/rest-client.service';
import { UiServiceService } from '../../../../services/ui-service.service';
let CompanySetupComponent = class CompanySetupComponent {
    constructor(restService, dialog, uiService) {
        this.restService = restService;
        this.dialog = dialog;
        this.uiService = uiService;
        this.displayedColumns = ['id', 'name_en', 'name_ar', 'logo'];
        this.dataSource = [];
    }
    ngOnInit() {
        this.loadCompanies();
    }
    loadCompanies() {
        this.restService.getCompanies().subscribe(response => {
            console.log(response);
            this.dataSource = response.result;
        });
    }
    onAddCompany() {
        const dialogRef = this.dialog.open(AddCompanyComponent, {
            width: '500px',
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadCompanies();
        });
    }
};
CompanySetupComponent = __decorate([
    Component({
        selector: 'app-company-setup',
        templateUrl: './company-setup.component.html',
        styleUrls: ['./company-setup.component.scss']
    }),
    __metadata("design:paramtypes", [RestService,
        MatDialog,
        UiServiceService])
], CompanySetupComponent);
export { CompanySetupComponent };
//# sourceMappingURL=company-setup.component.js.map