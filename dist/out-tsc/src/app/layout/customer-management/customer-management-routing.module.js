import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
const routes = [
    {
        path: '', component: CustomerManagementComponent
    }
];
let CustomerManagementRoutingModule = class CustomerManagementRoutingModule {
};
CustomerManagementRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CustomerManagementRoutingModule);
export { CustomerManagementRoutingModule };
//# sourceMappingURL=customer-management-routing.module.js.map