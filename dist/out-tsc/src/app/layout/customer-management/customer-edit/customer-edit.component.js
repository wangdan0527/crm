import { __decorate, __metadata, __param } from "tslib";
import { Component } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
let CustomerEditComponent = class CustomerEditComponent {
    constructor(restService, dialogRef, data) {
        this.restService = restService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.types = [];
        this.provinces = [];
        this.countries = [];
        this.cities = [];
        this.name_ar = "";
        this.type = -1;
        this.name_en = "";
        this.phone = "";
        this.fax = "";
        this.mobile = "";
        this.web = "";
        this.country = -1;
        this.province = -1;
        this.city = -1;
        this.address = "";
        this.mobile1 = "";
        this.mobile2 = "";
        this.company_email = "";
        this.co_representative = "";
        this.email = "";
        this.ipan_no = "";
        this.bank_acc = "";
        this.bank = "";
        this.branch = "";
        this.customer_classification = "";
        this.first_relation = null;
        this.last_relation = null;
        this.remarks = "";
    }
    ngOnInit() {
        this.loadData();
        this.customer = this.data;
        this.name_en = this.customer.name_en;
        this.name_ar = this.customer.name_ar;
        this.type = this.customer.type;
        this.phone = this.customer.phone;
        this.mobile = this.customer.mobile;
        this.fax = this.customer.fax;
        this.web = this.customer.web;
        this.country = this.customer.country;
        this.province = this.customer.province;
        this.city = this.customer.city;
        this.address = this.customer.address;
        this.mobile1 = this.customer.mobile1;
        this.mobile2 = this.customer.mobile2;
        this.email = this.customer.email;
        this.co_representative = this.customer.co_representative;
        this.company_email = this.customer.company_email;
        this.ipan_no = this.customer.ipan_no;
        this.bank_acc = this.customer.bank_acc;
        this.bank = this.customer.bank;
        this.branch = this.customer.branch;
        this.customer_classification = this.customer.customer_classification;
        this.first_relation = new Date(this.customer.first_relation * 1000);
        this.last_relation = new Date(this.customer.last_relation * 1000);
        this.remarks = this.customer.remarks;
    }
    loadData() {
        this.restService.getCountries().subscribe(response => {
            this.countries = response.countries;
        });
        this.restService.getProvinces().subscribe(response => {
            this.provinces = response.province;
            this.cities = response.city;
        });
        this.restService.getCustomerTypes().subscribe(response => {
            this.types = response.customerTypes;
        });
    }
    onSaveCustomer() {
        this.restService.editCustomer(this.customer.id, this.name_en, this.name_ar, this.type + "", this.phone, this.mobile, this.fax, this.web, this.country + "", this.province + "", this.city + "", this.address, this.mobile1, this.mobile2, this.email, this.co_representative, this.company_email, this.ipan_no, this.bank_acc, this.bank, this.branch, this.customer_classification, this.first_relation.getTime() / 1000 + "", this.last_relation.getTime() / 1000 + "", this.remarks).subscribe(response => {
            this.dialogRef.close();
        });
    }
};
CustomerEditComponent = __decorate([
    Component({
        selector: 'app-customer-edit',
        templateUrl: './customer-edit.component.html',
        styleUrls: ['./customer-edit.component.scss']
    }),
    __param(2, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [RestService,
        MatDialogRef, Object])
], CustomerEditComponent);
export { CustomerEditComponent };
//# sourceMappingURL=customer-edit.component.js.map