import { __decorate, __metadata, __param } from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
let CustomerDetailsComponent = class CustomerDetailsComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.first_relation = "";
        this.last_relation = "";
    }
    ngOnInit() {
        console.log(this.data);
        this.customer = this.data;
        let dateStart = new Date(this.customer.first_relation * 1000);
        this.first_relation = dateStart.toLocaleString();
        let dateLast = new Date(this.customer.last_relation * 1000);
        this.last_relation = dateLast.toLocaleString();
    }
};
CustomerDetailsComponent = __decorate([
    Component({
        selector: 'app-customer-details',
        templateUrl: './customer-details.component.html',
        styleUrls: ['./customer-details.component.scss']
    }),
    __param(1, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [MatDialogRef, Object])
], CustomerDetailsComponent);
export { CustomerDetailsComponent };
//# sourceMappingURL=customer-details.component.js.map