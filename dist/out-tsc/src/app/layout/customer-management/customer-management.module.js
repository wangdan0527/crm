import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
import { CustomerManagementRoutingModule } from './customer-management-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
let CustomerManagementModule = class CustomerManagementModule {
};
CustomerManagementModule = __decorate([
    NgModule({
        declarations: [CustomerAddComponent, CustomerManagementComponent, CustomerDetailsComponent, CustomerEditComponent],
        imports: [
            CommonModule,
            CustomerManagementRoutingModule,
            TranslateModule,
            MatDialogModule,
            MatButtonModule,
            MatFormFieldModule,
            MatInputModule,
            MatTableModule,
            FormsModule,
            MatSelectModule,
            MatDatepickerModule,
            MatNativeDateModule,
            MatSortModule,
            MatMenuModule,
            MatIconModule
        ],
        entryComponents: [
            CustomerAddComponent,
            CustomerDetailsComponent,
            CustomerEditComponent
        ]
    })
], CustomerManagementModule);
export { CustomerManagementModule };
//# sourceMappingURL=customer-management.module.js.map