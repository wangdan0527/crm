import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
const routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule) },
            { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
            { path: 'forms', loadChildren: () => import('./form/form.module').then(m => m.FormModule) },
            { path: 'bs-element', loadChildren: () => import('./bs-element/bs-element.module').then(m => m.BsElementModule) },
            { path: 'grid', loadChildren: () => import('./grid/grid.module').then(m => m.GridModule) },
            { path: 'components', loadChildren: () => import('./bs-component/bs-component.module').then(m => m.BsComponentModule) },
            { path: 'blank-page', loadChildren: () => import('./blank-page/blank-page.module').then(m => m.BlankPageModule) },
            { path: 'company-setup', loadChildren: () => import('./company-setup/company-setup.module').then(m => m.CompanySetupModule) },
            { path: 'user-setup', loadChildren: () => import('./user-setup/user-setup.module').then(m => m.UserSetupModule) },
            { path: 'province-setup', loadChildren: () => import('./province-setup/province-setup.module').then(m => m.ProvinceSetupModule) },
            { path: 'complaint-type-setup', loadChildren: () => import('./complaint-type-setup/complaint-type-setup.module').then(m => m.ComplaintTypeSetupModule) },
            { path: 'vendor-setup', loadChildren: () => import('./vendor-setup/vendor-setup.module').then(m => m.VendorSetupModule) },
            { path: 'customer-type-setup', loadChildren: () => import('./customer-type-setup/customer-type-setup.module').then(m => m.CustomerTypeSetupModule) },
            { path: 'customer-management', loadChildren: () => import('./customer-management/customer-management.module').then(m => m.CustomerManagementModule) },
            { path: 'vendor-management', loadChildren: () => import('./vendor-management/vendor-management.module').then(m => m.VendorManagementModule) },
            { path: 'department-management', loadChildren: () => import('./department-management/department-management.module').then(m => m.DepartmentManagementModule) }
        ]
    }
];
let LayoutRoutingModule = class LayoutRoutingModule {
};
LayoutRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], LayoutRoutingModule);
export { LayoutRoutingModule };
//# sourceMappingURL=layout-routing.module.js.map