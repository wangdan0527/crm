import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material';
let SignupModule = class SignupModule {
};
SignupModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            TranslateModule,
            SignupRoutingModule,
            FormsModule,
            MatSlideToggleModule,
            MatSelectModule,
            MatInputModule
        ],
        declarations: [SignupComponent]
    })
], SignupModule);
export { SignupModule };
//# sourceMappingURL=signup.module.js.map