import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
let UtilsService = class UtilsService {
    constructor() { }
    static calculateArea() {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < 10; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
};
UtilsService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [])
], UtilsService);
export { UtilsService };
//# sourceMappingURL=utils.service.js.map