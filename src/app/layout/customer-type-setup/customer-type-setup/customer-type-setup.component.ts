import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { CustomerTypeAddComponent } from '../customer-type-add/customer-type-add.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-customer-type-setup',
  templateUrl: './customer-type-setup.component.html',
  styleUrls: ['./customer-type-setup.component.scss']
})
export class CustomerTypeSetupComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name'];
  dataSource = new MatTableDataSource([]);
  users = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    public dialog: MatDialog) { }

  ngOnInit() {
  	this.loadCustomerTypes()    
  	this.dataSource.sort = this.sort;
  }

  loadCustomerTypes()
  {
  	this.restService.getCustomerTypes().subscribe(response => {
  		console.log(response)

  		this.dataSource = new MatTableDataSource(response.customerTypes);
        this.dataSource.sort = this.sort;
  	})
  }

  onAddCustomerType()
  {
  	const dialogRef = this.dialog.open(CustomerTypeAddComponent, {
	  width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
       this.loadCustomerTypes()
    });
  }
}
