import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-customer-type-add',
  templateUrl: './customer-type-add.component.html',
  styleUrls: ['./customer-type-add.component.scss']
})
export class CustomerTypeAddComponent implements OnInit {

  name : string = "";

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<CustomerTypeAddComponent>) { }

  ngOnInit() {
  }

  onSaveCustomerType()
  {
    if(!this.name)
    {
      return
    }

    this.restService.addCustomerType(this.name).subscribe(response => {
      this.dialogRef.close();
    });   
  }
}
