import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerTypeSetupComponent } from './customer-type-setup/customer-type-setup.component'
const routes: Routes = [
    {
        path: '', component: CustomerTypeSetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomerTypeSetupRoutingModule { }
