import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTypeSetupComponent } from './customer-type-setup/customer-type-setup.component';
import { CustomerTypeAddComponent } from './customer-type-add/customer-type-add.component';
import { CustomerTypeSetupRoutingModule } from './customer-type-setup-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatSortModule} from '@angular/material/sort';

@NgModule({
  declarations: [CustomerTypeSetupComponent, CustomerTypeAddComponent],
  imports: [
    CommonModule,    
    CustomerTypeSetupRoutingModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatSortModule
  ],
    entryComponents: [
      CustomerTypeAddComponent
  ]
})
export class CustomerTypeSetupModule { }
