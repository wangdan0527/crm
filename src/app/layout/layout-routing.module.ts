import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule) },
            { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
            { path: 'forms', loadChildren: () => import('./form/form.module').then(m => m.FormModule) },
            { path: 'bs-element', loadChildren: () => import('./bs-element/bs-element.module').then(m => m.BsElementModule) },
            { path: 'grid', loadChildren: () => import('./grid/grid.module').then(m => m.GridModule) },
            { path: 'components', loadChildren: () => import('./bs-component/bs-component.module').then(m => m.BsComponentModule) },
            { path: 'blank-page', loadChildren: () => import('./blank-page/blank-page.module').then(m => m.BlankPageModule) },
            { path: 'company-setup', loadChildren: () => import('./company-setup/company-setup.module').then(m => m.CompanySetupModule) },
            { path: 'user-setup', loadChildren: () => import('./user-setup/user-setup.module').then(m => m.UserSetupModule) },
            { path: 'province-setup', loadChildren: () => import('./province-setup/province-setup.module').then(m => m.ProvinceSetupModule) },
            { path: 'complaint-type-setup', loadChildren: () => import('./complaint-type-setup/complaint-type-setup.module').then(m => m.ComplaintTypeSetupModule) },
            { path: 'vendor-setup', loadChildren: () => import('./vendor-setup/vendor-setup.module').then(m => m.VendorSetupModule) },
            { path: 'customer-type-setup', loadChildren: () => import('./customer-type-setup/customer-type-setup.module').then(m => m.CustomerTypeSetupModule) },
            { path: 'customer-management', loadChildren: () => import('./customer-management/customer-management.module').then(m => m.CustomerManagementModule) },
            { path: 'vendor-management', loadChildren: () => import('./vendor-management/vendor-management.module').then(m => m.VendorManagementModule) },
            { path: 'department-management', loadChildren: () => import('./department-management/department-management.module').then(m => m.DepartmentManagementModule) },
            { path: 'engineer-management', loadChildren: () => import('./engineer-management/engineer-management.module').then(m => m.EngineerManagementModule) },
            { path: 'consultant-management', loadChildren: () => import('./consultant-management/consultant-management.module').then(m => m.ConsultantManagementModule) },
            { path: 'project-management', loadChildren: () => import('./project-management/project-management.module').then(m => m.ProjectManagementModule) },
            { path: 'employee-management', loadChildren: () => import('./employee-management/employee-management.module').then(m => m.EmployeeManagementModule) }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
