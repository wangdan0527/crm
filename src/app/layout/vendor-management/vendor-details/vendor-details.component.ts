import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-vendor-details',
  templateUrl: './vendor-details.component.html',
  styleUrls: ['./vendor-details.component.scss']
})
export class VendorDetailsComponent implements OnInit {

	vendor: any;
	first_relation: string = ""
	last_relation: string = ""
  	constructor(
	  	public dialogRef: MatDialogRef<VendorDetailsComponent>,
	  	@Inject(MAT_DIALOG_DATA) public data: any) { }

  	ngOnInit() {
	  	console.log(this.data)
	  	this.vendor = this.data
	  	let dateStart : Date = new Date(this.vendor.first_relation * 1000)
	  	this.first_relation = dateStart.toLocaleString()
	  	let dateLast : Date = new Date(this.vendor.last_relation * 1000)
	  	this.last_relation = dateLast.toLocaleString()
  	}

}
