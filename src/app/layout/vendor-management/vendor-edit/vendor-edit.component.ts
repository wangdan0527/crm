import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-vendor-edit',
  templateUrl: './vendor-edit.component.html',
  styleUrls: ['./vendor-edit.component.scss']
})
export class VendorEditComponent implements OnInit {


  private types = []
  private provinces = []
  private countries = []
  private cities = []

  private name_ar: string = ""
  private type: number = -1
  private name_en: string = ""
  private phone: string = ""
  private fax: string = ""
  private mobile: string = ""
  private web: string = ""
  private country: number = -1
  private province: number = -1
  private city: number =-1
  private address: string = ""
  private mobile1: string = ""
  private mobile2: string = ""
  private company_email: string = ""
  private co_representative: string = ""
  private email: string = ""
  private ipan_no: string = ""
  private bank_acc: string = ""
  private bank: string = ""
  private branch: string = ""
  private vendor_classification: string = ""
  private first_relation: Date = null
  private last_relation: Date = null
  private remarks: string = ""

	vendor: any;

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<VendorEditComponent>,
	@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  	this.loadData()
    this.vendor = this.data

  	this.name_en = this.vendor.name_en
  	this.name_ar = this.vendor.name_ar
  	this.type = this.vendor.type
  	this.phone = this.vendor.phone
  	this.mobile = this.vendor.mobile
  	this.fax = this.vendor.fax
  	this.web = this.vendor.web
  	this.country = this.vendor.country
  	this.province = this.vendor.province
  	this.city = this.vendor.city
  	this.address = this.vendor.address
  	this.mobile1 = this.vendor.mobile1
  	this.mobile2 = this.vendor.mobile2
  	this.email = this.vendor.email
  	this.co_representative = this.vendor.co_representative
  	this.company_email = this.vendor.company_email
  	this.ipan_no = this.vendor.ipan_no
  	this.bank_acc = this.vendor.bank_acc
  	this.bank = this.vendor.bank
  	this.branch = this.vendor.branch
  	this.vendor_classification = this.vendor.vendor_classification
  	this.first_relation = new Date(this.vendor.first_relation * 1000)
  	this.last_relation = new Date(this.vendor.last_relation * 1000)
    this.remarks = this.vendor.remarks
  }

  loadData()
  {
  	this.restService.getCountries().subscribe(response => {
  		this.countries = response.countries
  		
  	})
  	this.restService.getProvinces().subscribe(response => {
      this.provinces = response.province;
      this.cities = response.city;  		
  	})
  	this.restService.getVendorTypes().subscribe(response => {
  		this.types = response.vendorTypes
  	})
  }

  onSaveVendor()
  {
  	this.restService.editVendor(
    this.vendor.id,
  	this.name_en, 
		this.name_ar, 
		this.type + "",
		this.phone,
		this.mobile,
		this.fax,
		this.web,
		this.country + "",
		this.province + "",
		this.city + "",
		this.address,
		this.mobile1,
		this.mobile2,
		this.email,
		this.co_representative,
		this.company_email,
		this.ipan_no,
		this.bank_acc,
		this.bank,
		this.branch,
		this.vendor_classification,
		this.first_relation.getTime()/1000 + "",
		this.last_relation.getTime()/1000 + "",
		this.remarks
  	).subscribe(response => {
      this.dialogRef.close()
    })
  }

}
