import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorAddComponent } from './vendor-add/vendor-add.component';
import { VendorDetailsComponent } from './vendor-details/vendor-details.component';
import { VendorEditComponent } from './vendor-edit/vendor-edit.component';
import { VendorManagementComponent } from './vendor-management/vendor-management.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSortModule} from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { VendorManagementRoutingModule } from './vendor-management-routing.module';

@NgModule({
  declarations: [VendorAddComponent, VendorDetailsComponent, VendorEditComponent, VendorManagementComponent],
  imports: [
    CommonModule,
    VendorManagementRoutingModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule
  ],
    entryComponents: [
      VendorAddComponent,
      VendorDetailsComponent,
      VendorEditComponent
  ]
})
export class VendorManagementModule { }
