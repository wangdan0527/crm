import { Component, OnInit ,NgZone} from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RestService } from '../../../services/rest-client.service'

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
    animations: [routerTransition()]
})
export class TablesComponent implements OnInit {

  	service: RestService;
	private companies = [];
	private departments = [];
	private projects = [];

	private selectedCompany: string = "";
	private selectedDepartment: string = "";
	private selectedProject: any;
	private projectStatus: string = "";
	private nextStage: string = "";
	private comment: string = "";
	private comments = [];

    constructor(service : RestService, private ngZone: NgZone) {
    	this.service = service
    }

    ngOnInit() {
        
    }


   //  loadCompanies()
   //  {
   //  	this.service.getCompanies().subscribe((companies) => {

   //  		if(companies.length == 0)
   //  		{
   //  			return
   //  		}

			// this.ngZone.run( () => {

	  //   		this.companies = companies;	 
	  //   		console.log(this.companies)
		 //    });
	  //   })
   //  }

   //  onCompanySelected()
   //  {
   //  	this.departments = []
   //  	this.projects = []
   //  	this.selectedProject = null

   //  	this.loadDepartments()
   //  }

   //  loadDepartments()
   //  {
   //  	this.service.getDepartments(this.selectedCompany).subscribe((departments) => {

			// this.ngZone.run( () => {

	  //   		this.departments = departments;	 
	  //   		console.log(this.departments)
		 //    });
	  //   })
   //  }

   //  onDepartmentSelected()
   //  {
   //  	this.projects = []
   //  	this.selectedProject = null
   //  	this.loadProjects()
   //  }

   //  loadProjects()
   //  {
   //  	this.service.getProjects(this.selectedDepartment).subscribe((projects) => {

			// this.ngZone.run( () => {

	  //   		this.projects = projects;	 
	  //   		console.log(this.projects)
		 //    });
	  //   })
    	
   //  }

   //  onProjectSelected()
   //  {
   //  	console.log(this.selectedProject)
   //  	this.projectStatus = this.selectedProject.status
   //  	this.nextStage = this.selectedProject.next_stage
   //  	this.comments = this.selectedProject.comments.split(":::")
   //  	if(this.comments.length > 0)
   //  	{
   //  		this.comments = this.comments.slice(1)
   //  	}
   //  	this.comment = ""
   //  	console.log(this.projectStatus)
   //  }

   //  onUpdateProject()
   //  {    	
   //  	this.selectedProject.status = +this.projectStatus
   //  	this.selectedProject.next_stage = this.nextStage

   //  	this.service.update(this.selectedProject)
   //  }

   //  onAddComment()
   //  {
   //  	if(!this.comment)
   //  	{
   //  		return
   //  	}

   //  	this.selectedProject.comments += ":::" + this.comment
   //  	this.service.update(this.selectedProject)
   //  	this.comments.push(this.comment)
   //  	this.comment = "";
   //  }

}
