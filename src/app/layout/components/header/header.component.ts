import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../../../services/ui-service.service'
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public pushRightClass: string;
    public userName: string = ""
    constructor(
        private translate: TranslateService, 
        public router: Router,
        private uiSerivce: UiServiceService) {

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.pushRightClass = 'push-right';
        this.userName = localStorage.getItem("name");
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    changeLang(language: string) {
        const dom: any = document.querySelector('body');
        this.translate.use(language);
        if(language == "ar")
        {            
            dom.classList.add('rtl');
            // this.uiSerivce.orientationChange.emit("rtl")
        }
        else
        {
            dom.classList.remove('rtl');
            // this.uiSerivce.orientationChange.next("")
        }
    }
}
