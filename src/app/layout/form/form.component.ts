import { Component, OnInit, NgZone} from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RestService } from '../../../services/rest-client.service'

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})
export class FormComponent implements OnInit {

  	service: RestService;

	private companyName: string = "";
	private departmentName: string = "";
	private projectName: string = "";

	private selectedCompany : string = "";
	private selectedDepartmentCompany : string = "";
	private selectedDepartment: string = "";

	private companies = [];
	private departments = [];


    constructor(service : RestService, private ngZone: NgZone) {
    	this.service = service
    }

    ngOnInit() {
    }

   //  onAddCompany()
   //  {
   //  	if(!this.companyName)
   //  	{
   //  		return
   //  	}

	  //   this.service.createCompany(this.companyName).subscribe((company) => {

	  //     if(company.length == 0)
	  //     {
	  //       return
	  //     }

   //        this.loadCompanies();
	  //   })
   //  }

   //  loadCompanies()
   //  {
   //  	this.service.getCompanies().subscribe((companies) => {

   //  		if(companies.length == 0)
   //  		{
   //  			return
   //  		}

			// this.ngZone.run( () => {

	  //   		this.companies = companies;	 
	  //   		console.log(this.companies)
		 //    });
	  //   })
   //  }

   //  loadDepartments()
   //  {
   //  	this.service.getDepartments(this.selectedDepartmentCompany).subscribe((departments) => {

   //  		if(departments.length == 0)
   //  		{
   //  			return
   //  		}

			// this.ngZone.run( () => {

	  //   		this.departments = departments;	 
	  //   		console.log(this.departments)
		 //    });
	  //   })
   //  }

   //  onAddDepartment()
   //  {
   //  	console.log(this.selectedCompany)

   //  	if(!this.departmentName)
   //  	{
   //  		return
   //  	}

   //  	if(!this.selectedCompany)
   //  	{
   //  		return
   //  	}

   //  	this.service.createDepartment(this.selectedCompany, this.departmentName).subscribe((department) => {

	  //     	console.log(department);

			// this.ngZone.run( () => {
			// 	this.departmentName = "";
		 //    });

	  //   })

   //  }

   //  onCompanySelected()
   //  {
   //      this.departments = [];

   //  	this.loadDepartments()
   //  }

   //  onAddProject()
   //  {
   //  	if(!this.projectName)
   //  	{
   //  		return
   //  	}

   //  	if(!this.selectedDepartment)
   //  	{
   //  		return
   //  	}

   //  	this.service.createProject(this.selectedDepartment, this.projectName).subscribe((project) => {

	  //     	if(project.length == 0)
	  //     	{
	  //       	return
	  //     	}

	  //     	console.log(project);

	  //     	this.projectName = "";

	  //   })

   //      this.projectName = "";
   //  }
}
