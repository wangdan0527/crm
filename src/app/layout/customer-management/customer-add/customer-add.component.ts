import { Component, OnInit } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

  private types = []
  private provinces = []
  private countries = []
  private cities = []

  private name_ar: string = ""
  private type: number = -1
  private name_en: string = ""
  private phone: string = ""
  private fax: string = ""
  private mobile: string = ""
  private web: string = ""
  private country: number = -1
  private province: number = -1
  private city: number =-1
  private address: string = ""
  private mobile1: string = ""
  private mobile2: string = ""
  private company_email: string = ""
  private co_representative: string = ""
  private email: string = ""
  private ipan_no: string = ""
  private bank_acc: string = ""
  private bank: string = ""
  private branch: string = ""
  private customer_classification: string = ""
  private first_relation: Date = null
  private last_relation: Date = null
  private remarks: string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<CustomerAddComponent>) { }

  ngOnInit() {
  	this.loadData()
  }

  loadData()
  {
  	this.restService.getCountries().subscribe(response => {
  		this.countries = response.countries
  		
  	})
  	this.restService.getProvinces().subscribe(response => {
      this.provinces = response.province;
      this.cities = response.city;

      console.log(this.cities)
  		
  	})
  	this.restService.getCustomerTypes().subscribe(response => {
  		this.types = response.customerTypes
  	})
  }

  onSaveCustomer()
  {
  	this.restService.addCustomer(
    	this.name_en, 
  		this.name_ar, 
  		this.type + "",
  		this.phone,
  		this.mobile,
  		this.fax,
  		this.web,
  		this.country + "",
  		this.province + "",
  		this.city + "",
  		this.address,
  		this.mobile1,
  		this.mobile2,
  		this.email,
  		this.co_representative,
  		this.company_email,
  		this.ipan_no,
  		this.bank_acc,
  		this.bank,
  		this.branch,
  		this.customer_classification,
  		this.first_relation.getTime()/1000 + "",
  		this.last_relation.getTime()/1000 + "",
  		this.remarks
  	).subscribe(response => {
      this.dialogRef.close()
  	})
  }
}
