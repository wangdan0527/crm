import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {

  private types = []
  private provinces = []
  private countries = []
  private cities = []

  private name_ar: string = ""
  private type: number = -1
  private name_en: string = ""
  private phone: string = ""
  private fax: string = ""
  private mobile: string = ""
  private web: string = ""
  private country: number = -1
  private province: number = -1
  private city: number =-1
  private address: string = ""
  private mobile1: string = ""
  private mobile2: string = ""
  private company_email: string = ""
  private co_representative: string = ""
  private email: string = ""
  private ipan_no: string = ""
  private bank_acc: string = ""
  private bank: string = ""
  private branch: string = ""
  private customer_classification: string = ""
  private first_relation: Date = null
  private last_relation: Date = null
  private remarks: string = ""

	customer: any;

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<CustomerEditComponent>,
	@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  	this.loadData()
    this.customer = this.data

  	this.name_en = this.customer.name_en
  	this.name_ar = this.customer.name_ar
  	this.type = this.customer.type
  	this.phone = this.customer.phone
  	this.mobile = this.customer.mobile
  	this.fax = this.customer.fax
  	this.web = this.customer.web
  	this.country = this.customer.country
  	this.province = this.customer.province
  	this.city = this.customer.city
  	this.address = this.customer.address
  	this.mobile1 = this.customer.mobile1
  	this.mobile2 = this.customer.mobile2
  	this.email = this.customer.email
  	this.co_representative = this.customer.co_representative
  	this.company_email = this.customer.company_email
  	this.ipan_no = this.customer.ipan_no
  	this.bank_acc = this.customer.bank_acc
  	this.bank = this.customer.bank
  	this.branch = this.customer.branch
  	this.customer_classification = this.customer.customer_classification
  	this.first_relation = new Date(this.customer.first_relation * 1000)
  	this.last_relation = new Date(this.customer.last_relation * 1000)
    this.remarks = this.customer.remarks
  }

  loadData()
  {
  	this.restService.getCountries().subscribe(response => {
  		this.countries = response.countries
  		
  	})
  	this.restService.getProvinces().subscribe(response => {
      this.provinces = response.province;
      this.cities = response.city;  		
  	})
  	this.restService.getCustomerTypes().subscribe(response => {
  		this.types = response.customerTypes
  	})
  }

  onSaveCustomer()
  {
  	this.restService.editCustomer(
    this.customer.id,
  	this.name_en, 
		this.name_ar, 
		this.type + "",
		this.phone,
		this.mobile,
		this.fax,
		this.web,
		this.country + "",
		this.province + "",
		this.city + "",
		this.address,
		this.mobile1,
		this.mobile2,
		this.email,
		this.co_representative,
		this.company_email,
		this.ipan_no,
		this.bank_acc,
		this.bank,
		this.branch,
		this.customer_classification,
		this.first_relation.getTime()/1000 + "",
		this.last_relation.getTime()/1000 + "",
		this.remarks
  	).subscribe(response => {
      this.dialogRef.close()
    })
  }
}
