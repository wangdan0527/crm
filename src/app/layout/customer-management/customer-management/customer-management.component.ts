import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { CustomerAddComponent } from '../customer-add/customer-add.component'
import { CustomerDetailsComponent } from '../customer-details/customer-details.component'
import { CustomerEditComponent } from '../customer-edit/customer-edit.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  displayedColumns: string[] = [
  'id', 
  'name_en', 
  'type_name',
  'country_name_en', 
  'province_name_en',
  'city_name_en',
  'address',
  'phone',
  'mobile',
  'email',
  'menu' ];
  dataSource = new MatTableDataSource([]);
  users = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.loadCustomers();
        console.log("customer ng on init")

  }

  loadCustomers()
  {
    this.restService.getCustomers().subscribe(response => {
      this.dataSource = new MatTableDataSource(response.customers);
      this.dataSource.sort = this.sort;
    })
  }

  onAddCustomer()
  {
  	const dialogRef = this.dialog.open(CustomerAddComponent, {
	    width: '900px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadCustomers();
    });
  }

  onDetails(element: any, index: number)
  {
    console.log(element)

    const dialogRef = this.dialog.open(CustomerDetailsComponent, {
     width: '900px',
     data: element
    });

    dialogRef.afterClosed().subscribe(result => {
       // this.loadCustomerTypes()
    });
  }

  onEdit(element: any, index: number)
  {
    console.log(element)

    const dialogRef = this.dialog.open(CustomerEditComponent, {
     width: '900px',
     data: element
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadCustomers();
    });
  }

  onPrint()
  {
    window.print();
  }

  onExportPDF()
  {
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });
  }
}
