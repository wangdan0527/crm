import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
	customer: any;
	first_relation: string = ""
	last_relation: string = ""
  	constructor(
	  	public dialogRef: MatDialogRef<CustomerDetailsComponent>,
	  	@Inject(MAT_DIALOG_DATA) public data: any) { }

  	ngOnInit() {
	  	console.log(this.data)
	  	this.customer = this.data
	  	let dateStart : Date = new Date(this.customer.first_relation * 1000)
	  	this.first_relation = dateStart.toLocaleString()
	  	let dateLast : Date = new Date(this.customer.last_relation * 1000)
	  	this.last_relation = dateLast.toLocaleString()
  	}

}
