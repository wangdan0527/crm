import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { AddVendorComponent } from '../add-vendor/add-vendor.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-vendor-setup',
  templateUrl: './vendor-setup.component.html',
  styleUrls: ['./vendor-setup.component.scss']
})
export class VendorSetupComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name'];
  dataSource = new MatTableDataSource([]);
  users = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    public dialog: MatDialog) { }

  ngOnInit() {
  	this.loadVendors()    
  	this.dataSource.sort = this.sort;

  }

  loadVendors()
  {
  	this.restService.getVendorTypes().subscribe(response => {
  		console.log(response)

  		this.dataSource = new MatTableDataSource(response.vendorTypes);
        this.dataSource.sort = this.sort;
  	})
  }

  onAddVendorType()
  {
  	const dialogRef = this.dialog.open(AddVendorComponent, {
	  width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
       this.loadVendors()
    });
  }
}
