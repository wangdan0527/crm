import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorSetupRoutingModule } from './vendor-setup-routing.module';
import { VendorSetupComponent } from './vendor-setup/vendor-setup.component';
import { AddVendorComponent } from './add-vendor/add-vendor.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatSortModule} from '@angular/material/sort';

@NgModule({
  declarations: [VendorSetupComponent, AddVendorComponent],
  imports: [
    CommonModule,
    VendorSetupRoutingModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    FormsModule
  ],
    entryComponents: [
      AddVendorComponent
  ]
})
export class VendorSetupModule { }
