import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorSetupComponent } from './vendor-setup/vendor-setup.component';

const routes: Routes = [
    {
        path: '', component: VendorSetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VendorSetupRoutingModule {
}
