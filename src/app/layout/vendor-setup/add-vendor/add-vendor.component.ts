import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.scss']
})
export class AddVendorComponent implements OnInit {

  name : string = "";

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<AddVendorComponent>) { }

  ngOnInit() {
  }

  onSaveVendor()
  {
    if(!this.name)
    {
      return
    }

    this.restService.addVendorType(this.name).subscribe(response => {
      this.dialogRef.close();
    });   
  }

}
