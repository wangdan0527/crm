import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DepartmentManagementComponent } from './department-management/department-management.component';
import { DepartmentManagementRoutingModule } from './department-management-routing.module';
import { DepartmentAddComponent } from './department-add/department-add.component';
import { MatSelectModule } from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { DepartmentEditComponent } from './department-edit/department-edit.component';

@NgModule({
  declarations: [DepartmentManagementComponent, DepartmentAddComponent, DepartmentEditComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatSortModule,
    MatSelectModule,
    DepartmentManagementRoutingModule,
    MatMenuModule,
    MatIconModule
  ],
    entryComponents: [
      DepartmentAddComponent,
      DepartmentEditComponent
  ]
})
export class DepartmentManagementModule { }
