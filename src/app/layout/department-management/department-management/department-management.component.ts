import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { UiServiceService } from '../../../../services/ui-service.service'
import { DepartmentAddComponent } from '../department-add/department-add.component';
import { DepartmentEditComponent } from '../department-edit/department-edit.component';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-department-management',
  templateUrl: './department-management.component.html',
  styleUrls: ['./department-management.component.scss']
})
export class DepartmentManagementComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'company_info', 'menu'];
  dataSource = new MatTableDataSource([]);
  departments = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    private uiService: UiServiceService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.loadDepartments()
  }

  loadDepartments()
  {
    this.restService.getDepartments().subscribe(response => {
      this.setupTable(response)
    })
  }

  setupTable(response)
  {
      for(var i = 0; i < response.departments.length; i++)
      {
        response.departments[i].company_info = response.departments[i].company_name_e + " (" + response.departments[i].company_name_a + ")"
      }

      this.dataSource = new MatTableDataSource(response.departments);
      this.dataSource.sort = this.sort;
  }


  onAddDepartment()
  {
  	const dialogRef = this.dialog.open(DepartmentAddComponent, {
	    width: '600px',
    });

    dialogRef.afterClosed().subscribe(result => {
    	this.loadDepartments()
    });
  }

  onDelete(department, index)
  {
    this.uiService.openDeleteConfirmDialog().subscribe(response => {
      if(response == true)
      {
        this.restService.deleteDepartment(department.id).subscribe(result => {
          this.setupTable(result)
        })
      }
    })
  }

  onEdit(department, index)
  {
    const dialogRef = this.dialog.open(DepartmentEditComponent, {
      width: '600px',
      data : {
        id : department.id,
        name: department.name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadDepartments()
    });
  }
}
