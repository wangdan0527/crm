import { Component, OnInit, Inject } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.scss']
})
export class DepartmentEditComponent implements OnInit {

  id : string = ""
  name : string = ""
  errorMsg : string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<DepartmentEditComponent>,
	@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.id = this.data.id;
    this.name = this.data.name;
  }

  onSaveDepartment()
  {

  	if(!this.name)
  	{
  		this.errorMsg = "Please add name."
  		return
  	}

  	this.restService.editDepartment(this.id, this.name).subscribe(response => {
  		this.dialogRef.close()
  	})
  }

}
