import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddCompanyComponent } from '../add-company/add-company.component'
import { RestService } from '../../../../services/rest-client.service'
import { UiServiceService } from '../../../../services/ui-service.service'


@Component({
  selector: 'app-company-setup',
  templateUrl: './company-setup.component.html',
  styleUrls: ['./company-setup.component.scss']
})
export class CompanySetupComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name_en', 'name_ar', 'logo'];
  dataSource = [];

  constructor(
    private restService: RestService, 
    public dialog: MatDialog,
    private uiService: UiServiceService) { }

  ngOnInit() {

    this.loadCompanies()

  }

  loadCompanies()
  {
    this.restService.getCompanies().subscribe( response => {
      console.log(response)
      this.dataSource = response.result
    });
  }

  onAddCompany()
  {

  	const dialogRef = this.dialog.open(AddCompanyComponent, {
	  width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
       this.loadCompanies()
    });


  }

}
