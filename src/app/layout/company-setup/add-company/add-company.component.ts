import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UtilsService } from '../../../../environments/utils.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {

  name_ar : string = "";
  name_en : string = "";
  uploadPercent : number = 0;
  downloadURL: string;
  file : any;

  constructor(
  	private restService: RestService, 
  	private storage: AngularFireStorage,
    public dialogRef: MatDialogRef<AddCompanyComponent>) { }

  ngOnInit() {
  }


  onSaveCompany()
  {
  	if(!this.file)
  	{
  		return;
  	}

  	if(!this.name_ar)
  	{
  		return;
  	}

  	if(!this.name_en)
  	{
  		return;
  	}

    const filePath = UtilsService.calculateArea();
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, this.file);

    // observe percentage changes
    task.percentageChanges().subscribe(progress => {
    	console.log(progress)
    });
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() => 
        	fileRef.getDownloadURL().subscribe(result => {
        		this.downloadURL = result
        		this.registerCompany()
        	})
        )
     )
    .subscribe()
  }

  registerCompany()
  {
  	this.restService.registerCompany(this.name_en, this.name_ar, this.downloadURL)
  	.subscribe( response => {
       this.dialogRef.close();
    });
  }

  uploadFile(event) {

    this.file = event.target.files[0];

  }
}
