import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintTypeAddComponent } from './complaint-type-add.component';

describe('ComplaintTypeAddComponent', () => {
  let component: ComplaintTypeAddComponent;
  let fixture: ComponentFixture<ComplaintTypeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintTypeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintTypeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
