import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-complaint-type-add',
  templateUrl: './complaint-type-add.component.html',
  styleUrls: ['./complaint-type-add.component.scss']
})
export class ComplaintTypeAddComponent implements OnInit {

  name : string = "";

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<ComplaintTypeAddComponent>) { }

  ngOnInit() {
  }

  onSaveComplaintType()
  {
    if(!this.name)
    {
      return
    }

    this.restService.addComplaintType(this.name).subscribe(response => {
      this.dialogRef.close();
    });   
  }
}
