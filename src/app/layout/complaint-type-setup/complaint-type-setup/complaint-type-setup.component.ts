import { Component, OnInit ,ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { ComplaintTypeAddComponent } from '../complaint-type-add/complaint-type-add.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-complaint-type-setup',
  templateUrl: './complaint-type-setup.component.html',
  styleUrls: ['./complaint-type-setup.component.scss']
})
export class ComplaintTypeSetupComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name'];
  dataSource = new MatTableDataSource([]);
  users = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    public dialog: MatDialog) { }

  ngOnInit() {
  	this.loadComplaintTypes()    
  	this.dataSource.sort = this.sort;
  }

  loadComplaintTypes()
  {
  	this.restService.getComplaintTypes().subscribe(response => {

  		this.dataSource = new MatTableDataSource(response.vendorTypes);
      this.dataSource.sort = this.sort;
  	})
  }

  onAddVendorType()
  {
  	const dialogRef = this.dialog.open(ComplaintTypeAddComponent, {
	  width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
       this.loadComplaintTypes()
    });
  }
}
