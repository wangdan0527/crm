import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintTypeSetupComponent } from './complaint-type-setup.component';

describe('ComplaintTypeSetupComponent', () => {
  let component: ComplaintTypeSetupComponent;
  let fixture: ComponentFixture<ComplaintTypeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintTypeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintTypeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
