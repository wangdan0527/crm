import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintTypeSetupComponent } from './complaint-type-setup/complaint-type-setup.component'
const routes: Routes = [
    {
        path: '', component: ComplaintTypeSetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ComplaintTypeSetupRoutingModule { }
