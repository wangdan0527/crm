import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplaintTypeSetupComponent } from './complaint-type-setup/complaint-type-setup.component';
import { ComplaintTypeAddComponent } from './complaint-type-add/complaint-type-add.component';
import { ComplaintTypeSetupRoutingModule } from './complaint-type-setup-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatSortModule} from '@angular/material/sort';

@NgModule({
  declarations: [ComplaintTypeSetupComponent, ComplaintTypeAddComponent],
  imports: [
    CommonModule,
    ComplaintTypeSetupRoutingModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    FormsModule
  ],
    entryComponents: [
      ComplaintTypeAddComponent
  ]
})
export class ComplaintTypeSetupModule { }
