import { Component, OnInit ,ViewChild} from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { UiServiceService } from '../../../../services/ui-service.service'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-setup',
  templateUrl: './user-setup.component.html',
  styleUrls: ['./user-setup.component.scss']
})
export class UserSetupComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'email', 'company_name_e', 'approved', 'menu'];
  dataSource = new MatTableDataSource([]);
  users = []
  orientation: string = "";

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private uiSerivce: UiServiceService,
    private restService: RestService,
    private translate : TranslateService) { }

  ngOnInit() {
  	this.loadUsers()
    this.dataSource.sort = this.sort;

    if(this.translate.currentLang == "ar")
    {
      this.orientation = "rtl"
    }
    
    // this.uiSerivce.orientationChange.subscribe(orientation => {
    //   this.orientation = orientation;
    // })
  }

  loadUsers()
  {
  	this.restService.getUsers().subscribe( response => {
      this.dataSource = new MatTableDataSource(response.result);
      this.dataSource.sort = this.sort;
      this.users = response.result;
    });
  }

  onApprove(element: any)
  {
    this.restService.approveUser(element.user_id).subscribe(response => {

    })

    for(var i = 0; i < this.users.length; i++)
    {
      if(this.users[i].user_id == element.user_id)
      {
        this.users[i].approved = 1;
      }
    }
  }

  onDecline(element: any)
  {
    this.restService.declineUser(element.user_id).subscribe(response => {

    })

    for(var i = 0; i < this.users.length; i++)
    {
      if(this.users[i].user_id == element.user_id)
      {
        this.users[i].approved = 0;
      }
    }
  }
}
