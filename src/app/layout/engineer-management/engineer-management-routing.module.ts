import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EngineerManagementComponent } from './engineer-management/engineer-management.component'

const routes: Routes = [
    {
        path: '', component: EngineerManagementComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EngineerManagementRoutingModule { }
