import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { UiServiceService } from '../../../../services/ui-service.service'
import { EngineerAddComponent } from '../engineer-add/engineer-add.component';
import { EngineerEditComponent } from '../engineer-edit/engineer-edit.component';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-engineer-management',
  templateUrl: './engineer-management.component.html',
  styleUrls: ['./engineer-management.component.scss']
})
export class EngineerManagementComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'company_info', 'menu'];
  dataSource = new MatTableDataSource([]);
  employees = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    private uiService: UiServiceService, 
    public dialog: MatDialog) {

    }

  ngOnInit() {
    this.loadEngineers()
  }

  loadEngineers()
  {
    this.restService.getEngineers().subscribe(response => {
      this.setupTable(response)
    })
  }

  setupTable(response)
  {
      for(var i = 0; i < response.engineers.length; i++)
      {
        response.engineers[i].company_info = response.engineers[i].company_name_e + " (" + response.engineers[i].company_name_a + ")"
      }

      this.dataSource = new MatTableDataSource(response.engineers);
      this.dataSource.sort = this.sort;
  }


  onAddEngineer()
  {
  	const dialogRef = this.dialog.open(EngineerAddComponent, {
	    width: '600px',
    });

    dialogRef.afterClosed().subscribe(result => {
    	this.loadEngineers()
    });
  }

  onDelete(department, index)
  {
    this.uiService.openDeleteConfirmDialog().subscribe(response => {
      if(response == true)
      {
        this.restService.deleteEngineer(department.id).subscribe(result => {
          this.setupTable(result)
        })
      }
    })
  }

  onEdit(department, index)
  {
    const dialogRef = this.dialog.open(EngineerEditComponent, {
      width: '600px',
      data : {
        id : department.id,
        name: department.name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadEngineers()
    });
  }
}
