import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerManagementComponent } from './engineer-management.component';

describe('EngineerManagementComponent', () => {
  let component: EngineerManagementComponent;
  let fixture: ComponentFixture<EngineerManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineerManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
