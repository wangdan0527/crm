import { Component, OnInit } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-engineer-add',
  templateUrl: './engineer-add.component.html',
  styleUrls: ['./engineer-add.component.scss']
})
export class EngineerAddComponent implements OnInit {


  companies = []
  name : string = ""
  company : number = -1
  errorMsg : string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<EngineerAddComponent>) { }

  ngOnInit() {
  	this.loadCompanies()
  }

  loadCompanies()
  {
  	this.restService.getCompanies().subscribe(response => {
  		this.companies = response.result
  		console.log(this.companies)
  	})
  }

  onSaveEngineer()
  {
  	if(this.company == -1)
  	{
  		this.errorMsg = "Please select company."
  		return
  	}

  	if(!this.name)
  	{
  		this.errorMsg = "Please add name."
  		return
  	}

  	this.restService.addEngineer(this.name, this.company + "").subscribe(response => {
  		this.dialogRef.close()
  	})
  }
}
