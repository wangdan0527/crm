import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { EngineerManagementComponent } from './engineer-management/engineer-management.component';
import { EngineerManagementRoutingModule } from './engineer-management-routing.module';
import { EngineerAddComponent } from './engineer-add/engineer-add.component';
import { MatSelectModule } from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { EngineerEditComponent } from './engineer-edit/engineer-edit.component';

@NgModule({
  declarations: [EngineerManagementComponent, EngineerAddComponent, EngineerEditComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatSortModule,
    MatSelectModule,
    EngineerManagementRoutingModule,
    MatMenuModule,
    MatIconModule,
    MatSortModule
  ],
    entryComponents: [
      EngineerAddComponent,
      EngineerEditComponent
  ]
})
export class EngineerManagementModule { }
