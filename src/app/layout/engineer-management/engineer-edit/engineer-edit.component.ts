import { Component, OnInit, Inject } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-engineer-edit',
  templateUrl: './engineer-edit.component.html',
  styleUrls: ['./engineer-edit.component.scss']
})
export class EngineerEditComponent implements OnInit {

  id : string = ""
  name : string = ""
  errorMsg : string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<EngineerEditComponent>,
	@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.id = this.data.id;
    this.name = this.data.name;
  }

  onSaveEngineer()
  {

  	if(!this.name)
  	{
  		this.errorMsg = "Please add name."
  		return
  	}

  	this.restService.editEngineer(this.id, this.name).subscribe(response => {
  		this.dialogRef.close()
  	})
  }
}
