import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProvinceSetupComponent } from './province-setup/province-setup.component';

const routes: Routes = [
    {
        path: '', component: ProvinceSetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProvinceSetupRoutingModule {
}
