import { Component, OnInit, Inject } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-province',
  templateUrl: './add-province.component.html',
  styleUrls: ['./add-province.component.scss']
})
export class AddProvinceComponent implements OnInit {

  name_ar : string = "";
  name_en : string = "";

  country_id : any;
  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<AddProvinceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data.country_id)
  }

  onSaveProvince()
  {
    if(!this.name_ar)
    {
      return
    }
    if(!this.name_en)
    {
      return
    }

    this.restService.addProvince(this.name_en, this.name_ar, this.data.country_id).subscribe(response => {
      this.dialogRef.close();
    });    
  }

}
