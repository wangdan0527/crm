import { Component, OnInit, Inject } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})
export class AddCityComponent implements OnInit {


  name_ar : string = "";
  name_en : string = "";
  province : number = -1;
  provinces = []
  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<AddCityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  onSaveCity()
  {
    if(!this.name_ar)
    {
      return
    }
    if(!this.name_en)
    {
      return
    }

    this.restService.addCity(this.name_en, this.name_ar, this.data.province_id).subscribe(response => {
      this.dialogRef.close();
    });   
  }

}
