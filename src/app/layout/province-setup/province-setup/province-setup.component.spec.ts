import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvinceSetupComponent } from './province-setup.component';

describe('ProvinceSetupComponent', () => {
  let component: ProvinceSetupComponent;
  let fixture: ComponentFixture<ProvinceSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvinceSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvinceSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
