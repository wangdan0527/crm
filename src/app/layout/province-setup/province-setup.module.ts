import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProvinceSetupComponent } from './province-setup/province-setup.component';
import { ProvinceSetupRoutingModule } from './province-setup-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { AddProvinceComponent } from './add-province/add-province.component';
import { AddCityComponent } from './add-city/add-city.component';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import { AddCountryComponent } from './add-country/add-country.component';


@NgModule({
  declarations: [ProvinceSetupComponent, AddProvinceComponent, AddCityComponent, AddCountryComponent],
  imports: [
    CommonModule,
    ProvinceSetupRoutingModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    TranslateModule,
    FormsModule,
    CdkTreeModule,
    MatTreeModule,
    MatIconModule
  ],
    entryComponents: [
      AddProvinceComponent,
      AddCityComponent,
      AddCountryComponent
  ]
})
export class ProvinceSetupModule { }
