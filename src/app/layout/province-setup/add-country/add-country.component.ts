import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent implements OnInit {

  name_ar : string = "";
  name_en : string = "";

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<AddCountryComponent>) { }

  ngOnInit() {
  }

  onSaveProvince()
  {
    if(!this.name_ar)
    {
      return
    }
    if(!this.name_en)
    {
      return
    }

    this.restService.addCountry(this.name_en, this.name_ar).subscribe(response => {
      this.dialogRef.close();
    });    
  }

}
