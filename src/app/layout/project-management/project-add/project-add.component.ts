import { Component, OnInit } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PROJECT_STAGE, PROJECT_STATUS } from '../../../../services/constants.service'
@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.scss']
})
export class ProjectAddComponent implements OnInit {

  private name: string = ""
  private start_date: Date = null
  private end_date: Date = null
  private location: string = ""
  private engineer: number = -1
  private owner: number = -1
  private consultant: number = -1
  private land_aria: string = ""
  private status: number = -1
  private stage: number = -1
  private remarks: string = ""

  private stages = []
  private statues = []
  private customers = []
  private consultants = []
  private engineers = []

  private errorMsg = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<ProjectAddComponent>) { }

  ngOnInit() {

  	this.stages = PROJECT_STAGE
  	this.statues = PROJECT_STATUS

  	this.loadConsultants()
  	this.loadEngineers()
  	this.loadCustomers()
  }

  loadConsultants()
  {
    this.restService.getConsultants().subscribe(response => {
      this.consultants = response.consultants
    })
  }

  loadEngineers()
  {
    this.restService.getEngineers().subscribe(response => {
      this.engineers = response.engineers
    })
  }

  loadCustomers()
  {
    this.restService.getCustomers().subscribe(response => {
      this.customers = response.customers
      console.log(this.customers)
    })
  }


  onSaveProject()
  {
  	if(!this.name)
  	{
  		this.errorMsg = "Please add name."
  		return
  	}

  	if(!this.location)
  	{
  		this.errorMsg = "Please add location."
  		return
  	}

  	if(this.engineer == -1)
  	{
  		this.errorMsg = "Please add engineer."
  		return
  	}

  	if(this.owner == -1)
  	{
  		this.errorMsg = "Please add owner."
  		return
  	}

  	if(this.consultant == -1)
  	{
  		this.errorMsg = "Please add consultant."
  		return
  	}

  	if(this.status == -1)
  	{
  		this.errorMsg = "Please add status."
  		return
  	}

  	if(this.stage == -1)
  	{
  		this.errorMsg = "Please add stage."
  		return
  	}

  	this.restService.addProject(
    	this.name, 
  		this.start_date.getTime() / 1000 + "", 
  		this.end_date.getTime() / 1000 + "",
  		this.location,
  		this.engineer + "",
  		this.owner + "",
  		this.consultant + "",
  		this.land_aria,
  		this.status + "",
  		this.stage + "",
  		this.remarks
  	).subscribe(response => {
      // this.dialogRef.close()
  	})
  }

}
