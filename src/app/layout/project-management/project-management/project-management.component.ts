import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { ProjectAddComponent } from '../project-add/project-add.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { PROJECT_STAGE, PROJECT_STATUS } from '../../../../services/constants.service'
import { UiServiceService } from '../../../../services/ui-service.service'
import { ProjectEditComponent } from '../project-edit/project-edit.component';
import { ProjectDetailsComponent } from '../project-details/project-details.component';

@Component({
  selector: 'app-project-management',
  templateUrl: './project-management.component.html',
  styleUrls: ['./project-management.component.scss']
})
export class ProjectManagementComponent implements OnInit {

  displayedColumns: string[] = [
  'id', 
  'name', 
  'start_date_format',
  'end_date_format', 
  'customer',
  'engineer',
  'consultant',
  'location',
  'status_name',
  'stage_name',
  'menu'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  projects = []
  constructor(
    private restService: RestService, 
    public dialog: MatDialog,
    private uiService: UiServiceService) { }

  ngOnInit() {
    this.loadProjects()
  }

  loadProjects()
  {
    this.restService.getProjects().subscribe(response => {
      this.projects = response.projects
      for(var i = 0; i < this.projects.length; i++)
      {
        this.projects[i].start_date_format = this.uiService.formatDate(new Date(this.projects[i].start_date * 1000))
        this.projects[i].end_date_format = this.uiService.formatDate(new Date(this.projects[i].end_date * 1000))
        this.projects[i].status_name = PROJECT_STATUS[this.projects[i].status - 1].name
        this.projects[i].stage_name = PROJECT_STAGE[this.projects[i].stage - 1].name
      }

      this.dataSource = new MatTableDataSource(this.projects);
      this.dataSource.sort = this.sort;
    })
  }


  onAddProject()
  {
  	const dialogRef = this.dialog.open(ProjectAddComponent, {
	    width: '900px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadProjects();
    });
  }

  onDetails(element: any, index: number)
  {
    const dialogRef = this.dialog.open(ProjectDetailsComponent, {
     width: '900px',
     data: element
    });

    dialogRef.afterClosed().subscribe(result => {
       // this.loadCustomerTypes()
    });
  }

  onEdit(element: any, index: number)
  {
    const dialogRef = this.dialog.open(ProjectEditComponent, {
     width: '900px',
     data: element
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadProjects();
    });
  }
}
