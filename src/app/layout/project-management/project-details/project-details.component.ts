import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { PROJECT_STAGE, PROJECT_STATUS } from '../../../../services/constants.service'

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  private name: string = ""
  private start_date: Date = null
  private end_date: Date = null
  private location: string = ""
  private engineer: number = -1
  private owner: number = -1
  private consultant: number = -1
  private land_aria: string = ""
  private status: number = -1
  private stage: number = -1
  private remarks: string = ""

  private stages = []
  private statues = []
  private customers = []
  private consultants = []
  private engineers = []

  private errorMsg = ""

	project: any;

  	constructor(
	  	private restService: RestService, 
	    public dialogRef: MatDialogRef<ProjectDetailsComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

  	ngOnInit() {
    	this.project = this.data

      this.name = this.project.name
      this.start_date = new Date(this.project.start_date * 1000)
      this.end_date = new Date(this.project.end_date * 1000)
      this.location = this.project.location
      this.engineer = this.project.engineer_id
      this.owner = this.project.owner_id
      this.consultant = this.project.consultant_id
      this.land_aria = this.project.land_aria
      this.status = parseInt(this.project.status)
      this.stage = parseInt(this.project.stage)
      this.remarks = this.project.remarks

      console.log(this.project)

      this.loadConsultants()
      this.loadEngineers()
      this.loadCustomers()


      this.stages = PROJECT_STAGE
      this.statues = PROJECT_STATUS
  	}

  loadConsultants()
  {
    this.restService.getConsultants().subscribe(response => {
      this.consultants = response.consultants
    })
  }

  loadEngineers()
  {
    this.restService.getEngineers().subscribe(response => {
      this.engineers = response.engineers
    })
  }

  loadCustomers()
  {
    this.restService.getCustomers().subscribe(response => {
      this.customers = response.customers
    })
  }

}
