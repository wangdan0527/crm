import { Component, OnInit } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { GENDER, JOB, UNIVERSITY, BANK, MANAGEMENT } from '../../../../services/constants.service'

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent implements OnInit {


  private genders = []
  private jobs = []
  private universities = []
  private banks = []
  private managements = []
  private companies = []

  private provinces = []
  private countries = []
  private cities = []

  private name_ar: string = ""
  private name_en: string = ""
  private gender: number = -1
  private nationality: string = ""
  private job_type: string = ""

  private dob: Date = null
  private degree: string = ""
  private university: number = -1
  private job: number = -1
  private mobile_no: string = ""
  private email: string = ""

  private country: number = -1
  private province: number = -1
  private city: number =-1

  private aria: string = ""
  private house_no: string = ""
  private mobile_no1: string = ""
  private mobile_no2: string = ""
  private office_email: string = ""
  private id_no: string = ""
  private exp_date: Date = null
  private passport_no: string = ""
  private passport_exp_date: Date = null
  private insurance_no: string = ""
  private insurance_exp_date: Date = null

  private ipan_no: string = ""
  private acc_no: string = ""
  private bank: number =-1
  private branch: string = ""
  private basic_salary: string = ""
  private total_salary: string = ""
  private start_date: Date = null
  private end_date: Date = null
  private job_status: string = ""

  private management: number =-1
  private company: number =-1
  private job_id: string = ""

  private errorMsg: string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<EmployeeAddComponent>) { }

  ngOnInit() {
  	this.loadData()

  	this.genders = GENDER
  	this.jobs = JOB
  	this.universities = UNIVERSITY
  	this.banks = BANK
  	this.managements = MANAGEMENT
  }

  loadData()
  {
  	this.restService.getCountries().subscribe(response => {
  		this.countries = response.countries
  		
  	})
  	this.restService.getProvinces().subscribe(response => {
      this.provinces = response.province;
      this.cities = response.city;  		
  	})
  	this.restService.getCompanies().subscribe(response => {
  		this.companies = response.result
  	})
  }

  onSaveEmployee()
  {
  	if(this.company == -1)
  	{
  		this.errorMsg = "Please select company."
  		return
  	}

  	if(this.gender == -1)
  	{
  		this.errorMsg = "Please select gender."
  		return
  	}

  	if(this.job == -1)
  	{
  		this.errorMsg = "Please select job."
  		return
  	}

  	if(!this.name_ar)
  	{
  		this.errorMsg = "Please add Arabic name."
  		return
  	}

  	if(!this.name_en)
  	{
  		this.errorMsg = "Please add English name."
  		return
  	}

  	if(!this.dob)
  	{
  		this.errorMsg = "Please add date of birth."
  		return
  	}

  	if(!this.degree)
  	{
  		this.errorMsg = "Please add degree."
  		return
  	}

  	if(!this.mobile_no)
  	{
  		this.errorMsg = "Please add mobile number."
  		return
  	}

  	if(!this.id_no)
  	{
  		this.errorMsg = "Please add id No."
  		return
  	}

  	this.restService.addEmployee(
    	this.name_ar,
    	this.name_en,
    	this.university + "",
    	this.mobile_no,
    	this.job_type,
    	this.gender + "",
    	this.dob.getTime()/1000 + "",
    	this.job + "",
    	this.email,
    	this.nationality,
    	this.degree,
    	this.country + "",
    	this.province + "",
    	this.city + "",
    	this.aria,
    	this.house_no,
    	this.mobile_no1,
    	this.mobile_no2,
    	this.office_email,
    	this.id_no,
    	this.exp_date.getTime()/1000 + "",
    	this.passport_no,
    	this.passport_exp_date.getTime()/1000 + "",
    	this.insurance_no,
    	this.insurance_exp_date.getTime()/1000 + "",
    	this.ipan_no,
    	this.acc_no,
    	this.bank + "",
    	this.branch,
    	this.basic_salary,
    	this.total_salary,
    	this.start_date.getTime()/1000 + "",
    	this.end_date.getTime()/1000 + "",
    	this.job_status,
    	this.management + "",
    	this.job_id,
    	this.company + ""
  	).subscribe(response => {
      // this.dialogRef.close()
  	})
  }
}
