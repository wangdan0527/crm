import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { UiServiceService } from '../../../../services/ui-service.service'
import { EmployeeAddComponent } from '../employee-add/employee-add.component'
import { EmployeeDetailsComponent } from '../employee-details/employee-details.component'
import { EmployeeEditComponent } from '../employee-edit/employee-edit.component'
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { GENDER, JOB, UNIVERSITY, BANK, MANAGEMENT } from '../../../../services/constants.service'

@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.component.html',
  styleUrls: ['./employee-management.component.scss']
})
export class EmployeeManagementComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name_ar', 'name_en', 'gender', 'nationality', 'id_no', 'menu'];
  dataSource = new MatTableDataSource([]);
  employees = []
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    private uiService: UiServiceService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.loadEmployees()
  }

  loadEmployees()
  {
    this.restService.getEmployees().subscribe(response => {
      this.setupTable(response)
    })
  }

  setupTable(response)
  {
      for(var i = 0; i < response.employees.length; i++)
      {
        response.employees[i].gender_name = GENDER[response.employees[i].gender - 1].name
      }

      console.log(response.employees)

      this.dataSource = new MatTableDataSource(response.employees);
      this.dataSource.sort = this.sort;
  }

  onAddEmployee()
  {
  	const dialogRef = this.dialog.open(EmployeeAddComponent, {
	    width: '900px',
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.loadCustomers();
    });
  }
}
