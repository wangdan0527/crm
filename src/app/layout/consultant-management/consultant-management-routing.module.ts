import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ConsultantManagementComponent } from './consultant-management/consultant-management.component'

const routes: Routes = [
    {
        path: '', component: ConsultantManagementComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConsultantManagementRoutingModule { }
