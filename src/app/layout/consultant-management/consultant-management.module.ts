import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ConsultantManagementComponent } from './consultant-management/consultant-management.component';
import { ConsultantManagementRoutingModule } from './consultant-management-routing.module';
import { ConsultantAddComponent } from './consultant-add/consultant-add.component';
import { MatSelectModule } from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { ConsultantEditComponent } from './consultant-edit/consultant-edit.component';

@NgModule({
  declarations: [ConsultantManagementComponent, ConsultantAddComponent, ConsultantEditComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatSortModule,
    MatSelectModule,
    ConsultantManagementRoutingModule,
    MatMenuModule,
    MatIconModule
  ],
    entryComponents: [
      ConsultantAddComponent,
      ConsultantEditComponent
  ]
})
export class ConsultantManagementModule { }
