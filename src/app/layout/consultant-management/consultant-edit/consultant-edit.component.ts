import { Component, OnInit, Inject } from '@angular/core';

import { RestService } from '../../../../services/rest-client.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-consultant-edit',
  templateUrl: './consultant-edit.component.html',
  styleUrls: ['./consultant-edit.component.scss']
})
export class ConsultantEditComponent implements OnInit {

  id : string = ""
  name : string = ""
  errorMsg : string = ""

  constructor(
  	private restService: RestService, 
    public dialogRef: MatDialogRef<ConsultantEditComponent>,
	@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.id = this.data.id;
    this.name = this.data.name;
  }

  onSaveConsultant()
  {

  	if(!this.name)
  	{
  		this.errorMsg = "Please add name."
  		return
  	}

  	this.restService.editConsultant(this.id, this.name).subscribe(response => {
  		this.dialogRef.close()
  	})
  }

}
