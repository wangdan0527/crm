import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestService } from '../../../../services/rest-client.service'
import { UiServiceService } from '../../../../services/ui-service.service'
import { ConsultantAddComponent } from '../consultant-add/consultant-add.component';
import { ConsultantEditComponent } from '../consultant-edit/consultant-edit.component';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-consultant-management',
  templateUrl: './consultant-management.component.html',
  styleUrls: ['./consultant-management.component.scss']
})
export class ConsultantManagementComponent implements OnInit {


  displayedColumns: string[] = ['id', 'name', 'company_info', 'menu'];
  dataSource = new MatTableDataSource([]);
  consultants = []

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private restService: RestService, 
    private uiService: UiServiceService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.loadConsultants()
  }

  loadConsultants()
  {
    this.restService.getConsultants().subscribe(response => {
      console.log(response)
      this.setupTable(response)
    })
  }

  setupTable(response)
  {
      for(var i = 0; i < response.consultants.length; i++)
      {
        response.consultants[i].company_info = response.consultants[i].company_name_e + " (" + response.consultants[i].company_name_a + ")"
      }

      this.dataSource = new MatTableDataSource(response.consultants);
      this.dataSource.sort = this.sort;
  }


  onAddConsultant()
  {
  	const dialogRef = this.dialog.open(ConsultantAddComponent, {
	    width: '600px',
    });

    dialogRef.afterClosed().subscribe(result => {
    	this.loadConsultants()
    });
  }

  onDelete(department, index)
  {
    this.uiService.openDeleteConfirmDialog().subscribe(response => {
      if(response == true)
      {
        this.restService.deleteConsultant(department.id).subscribe(result => {
          this.setupTable(result)
        })
      }
    })
  }

  onEdit(department, index)
  {
    const dialogRef = this.dialog.open(ConsultantEditComponent, {
      width: '600px',
      data : {
        id : department.id,
        name: department.name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadConsultants()
    });
  }

}
