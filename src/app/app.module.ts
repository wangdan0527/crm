import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { AngularFireModule } from '@angular/fire';
import {AngularFireStorageModule, StorageBucket} from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { PopUpComponent } from './ui/pop-up/pop-up.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireStorageModule,
        MatDialogModule
    ],
    declarations: [AppComponent, PopUpComponent],
    providers: [
        AuthGuard,
        { provide: StorageBucket, useValue: environment.firebaseConfig.storageBucket }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        PopUpComponent, 
    ]
})
export class AppModule {}
