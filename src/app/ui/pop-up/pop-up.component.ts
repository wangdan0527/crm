import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PopUpComponent>) { }

  ngOnInit() {
  }

  onClose()
  {
  	this.dialogRef.close(0)

  }

  onDelete()
  {
  	this.dialogRef.close(1)
  }

}
