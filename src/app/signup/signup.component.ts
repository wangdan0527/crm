import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { RestService } from '../../services/rest-client.service'
import { Router } from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {

    private name: string = "";
    private email: string = "";
    private pwd: string = "";
    private pwdConfirm: string = "";
    private bIfAdmin: boolean = false;
    private companies = [];
    private selectedCompany : any;

    private errorMessage: string = ""
    
    constructor(
    	private restService : RestService,
        private router: Router
	) {}

    ngOnInit() {

        this.loadCompanies()
    }

    loadCompanies()
    {
        this.restService.getCompanies().subscribe( response => {
          this.companies = response.result

          console.log(this.companies)
        });
    }

    onRegister()
    {
        if(!this.name)
        {
            this.errorMessage = "Please enter name.";
            return 
        }

        if(!this.email)
        {
            this.errorMessage = "Please enter email.";
            return 
        }

        if(!this.pwd)
        {
            this.errorMessage = "Please enter message.";
            return 
        }

        if(this.pwd.length < 5)
        {
            this.errorMessage = "Please enter password longer than 5."
        }

        if(!this.pwdConfirm)
        {
            this.errorMessage = "Please enter confirm password.";
            return 
        }

        if(this.pwd != this.pwdConfirm)
        {
            this.errorMessage = "Passwords does not match.";
            return 
        }

        if(!this.bIfAdmin && !this.selectedCompany)
        {
            return ;
        }

        if(this.bIfAdmin)
        {
            this.restService.registerAdmin(
                this.name,
                this.email,
                this.pwd
            )
            .subscribe( response => {
                if(response.result == true)
                {
                    this.router.navigate(['/login']);
                }
            });
        }
        else
        {
            this.restService.registerUser(
                this.name,
                this.email,
                this.pwd,
                this.selectedCompany
            )
            .subscribe( response => {
                if(response.result == true)
                {
                    this.router.navigate(['/login']);
                }
            });
        }

    }
}
