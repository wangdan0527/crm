import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { RestService } from '../../services/rest-client.service'
import { Router } from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    private email: string = "";
    private pwd: string = "";

    private errorMessage: string = ""

    constructor(
        private restService : RestService,
        private router: Router
    ) {}

    ngOnInit() {}

    onLogin() {
        if(!this.email)
        {
            this.errorMessage = "Please enter email."
            return
        }

        if(!this.pwd)
        {
            this.errorMessage = "Please enter password."
            return
        }


        this.restService.loginAdmin(
            this.email,
            this.pwd
        )
        .subscribe( response => {
            console.log(response)
            if(response.result == true)
            {
                localStorage.setItem("isLoggedin", 'true')
                localStorage.setItem("token", response.data.token)
                localStorage.setItem("name", response.data.user.name)
                localStorage.setItem("email", response.data.user.email)

                this.router.navigate(['/company-setup']);
            }
            else if(response.result == false)
            {
                if(response.code == 1)
                {
                    this.errorMessage = "Please verify your account.";
                }
                else if(response.code == 2)
                {
                    this.errorMessage = "Invalid username or password.";
                }
            }
        });
    }
}
