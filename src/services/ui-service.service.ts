import { Injectable, Output, EventEmitter } from '@angular/core'
import { MatDialog } from '@angular/material';
import { PopUpComponent } from '../app/ui/pop-up/pop-up.component'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiServiceService {

  	constructor(private dialog : MatDialog) { }

  	openDeleteConfirmDialog(): BehaviorSubject<boolean> {
      var subject = new BehaviorSubject(false)

    	const dialogRef = this.dialog.open(PopUpComponent, {
	  		width: '500px',
    	});

    	dialogRef.afterClosed().subscribe(result => {
        if(result == 1)
        {
          subject.next(true)
        }
    	});

      return subject
    }

    formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) 
          month = '0' + month;
      if (day.length < 2) 
          day = '0' + day;

      return [year, month, day].join('-');
  }
}
