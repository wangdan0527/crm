

export const PROJECT_STATUS = [
	{
		id: 1,
		name: "PENDING"
	},
	{
		id: 2,
		name: "DECLINED"
	},
	{
		id: 3,
		name: "APPROVED"
	},
	{
		id: 4,
		name: "IN-PROGRESS"
	}
];

export const PROJECT_STAGE = [
	{
		id: 1,
		name: "IN-DISCUSSION"
	},
	{
		id: 2,
		name: "IN-PROGRESS"
	},
	{
		id: 3,
		name: "QA TEST"
	},
	{
		id: 4,
		name: "DELIVERING"
	}
];

export const GENDER = [
	{
		id: 1,
		name: "FEMALE"
	},
	{
		id: 2,
		name: "MALE"
	}
];

export const UNIVERSITY = [
	{
		id: 1,
		name: "Yale University"
	},
	{
		id: 2,
		name: "Bejing University"
	},
	{
		id: 2,
		name: "Bejing University"
	}
];

export const JOB = [
	{
		id: 1,
		name: "Software Engineer"
	},
	{
		id: 2,
		name: "QA Tester"
	},
	{
		id: 3,
		name: "Graphics Designer"
	}
];

export const BANK = [
	{
		id: 1,
		name: "China Agricultural Bank"
	},
	{
		id: 2,
		name: "New York Bank"
	},
	{
		id: 3,
		name: "Dubai Bank"
	}
];

export const MANAGEMENT = [
	{
		id: 1,
		name: "IT Department"
	},
	{
		id: 2,
		name: "Delivery Department"
	}
];