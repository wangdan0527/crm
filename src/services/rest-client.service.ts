import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { AppSettings } from '../environments/app-settings.service'
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class RestService {

  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  registerAdmin(name: string, email: string, password: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      formData.append('password', password);
      formData.append('type', "1");

      return this.http.post(environment.endpoint + 'account/register', formData).pipe(
      map(this.extractData));
  }

  registerUser(name: string, email: string, password: string, company_id: string): Observable<any> {
      console.log(company_id)
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      formData.append('password', password);
      formData.append('type', "0");
      formData.append('company_id', company_id);

      return this.http.post(environment.endpoint + 'account/register', formData).pipe(
      map(this.extractData));
  }

  loginAdmin(email: string, password: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('email', email);
      formData.append('password', password);

      return this.http.post(environment.endpoint + 'account/login', formData).pipe(
      map(this.extractData));
  }

  registerCompany(nameEn: string, nameAr: string, logoUrl: string): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));


      let formData: FormData = new FormData();
      formData.append('name_en', nameEn);
      formData.append('name_ar', nameAr);
      formData.append('logo_url', logoUrl);

      return this.http.post(environment.endpoint + 'company/register', formData, {headers : headers}).pipe(
      map(this.extractData));
  }

  getCompanies(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'company/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  getUsers(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'admin/users', {headers : headers}).pipe(
      map(this.extractData));
  }

  approveUser(user_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', user_id);

      return this.http.post(environment.endpoint + 'admin/user/approve', formData).pipe(
      map(this.extractData));
  }

  declineUser(user_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', user_id);

      return this.http.post(environment.endpoint + 'admin/user/decline', formData).pipe(
      map(this.extractData));
  }

  addProvince(name_en: string, name_ar: string, country_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('country_id', country_id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'province', formData, {headers: headers}).pipe(
      map(this.extractData));
  }


  addCountry(name_en: string, name_ar: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'country', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  addCity(name_en: string, name_ar: string, province: number): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('province_id', province + "");

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'city', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getProvinces(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'province/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  getCountries(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'country/list', {headers : headers}).pipe(
      map(this.extractData));
  }


  addVendorType(name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'vendor_type', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getVendorTypes(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'vendor_type/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addComplaintType(name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'complaint_type', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getComplaintTypes(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'complaint_type/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addCustomerType(name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'customer_type', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getCustomerTypes(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'customer_type/list', {headers : headers}).pipe(
      map(this.extractData));
  }


  addCustomer(
    name_en: string,
    name_ar: string,
    type: string,
    phone: string,
    mobile: string,
    fax: string,
    web: string,
    country: string,
    province: string,
    city: string,
    address: string,
    mobile1: string,
    mobile2: string,
    email: string,
    co_representative: string,
    company_email: string,
    ipan_no: string,
    bank_acc: string,
    bank: string,
    branch: string,
    customer_classification: string,
    first_relation: string,
    last_relation: string,
    remarks: string
    ): Observable<any> {
      let formData: FormData = new FormData();

      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('type', type);
      formData.append('phone', phone);
      formData.append('mobile', mobile);
      formData.append('fax', fax);
      formData.append('web', web);
      formData.append('country', country);
      formData.append('province', province);
      formData.append('city', city);
      formData.append('address', address);
      formData.append('mobile1', mobile1);
      formData.append('mobile2', mobile2);
      formData.append('email', email);
      formData.append('co_representative', co_representative);
      formData.append('company_email', company_email);
      formData.append('ipan_no', ipan_no);
      formData.append('bank_acc', bank_acc);
      formData.append('bank', bank);
      formData.append('branch', branch);
      formData.append('customer_classification', customer_classification);
      formData.append('first_relation', first_relation);
      formData.append('last_relation', last_relation);
      formData.append('remarks', remarks);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'customer', formData, {headers: headers}).pipe(
      map(this.extractData));
  }


  editCustomer(
    id: string,
    name_en: string,
    name_ar: string,
    type: string,
    phone: string,
    mobile: string,
    fax: string,
    web: string,
    country: string,
    province: string,
    city: string,
    address: string,
    mobile1: string,
    mobile2: string,
    email: string,
    co_representative: string,
    company_email: string,
    ipan_no: string,
    bank_acc: string,
    bank: string,
    branch: string,
    customer_classification: string,
    first_relation: string,
    last_relation: string,
    remarks: string
    ): Observable<any> {
      let formData: FormData = new FormData();

      formData.append('id', id);
      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('type', type);
      formData.append('phone', phone);
      formData.append('mobile', mobile);
      formData.append('fax', fax);
      formData.append('web', web);
      formData.append('country', country);
      formData.append('province', province);
      formData.append('city', city);
      formData.append('address', address);
      formData.append('mobile1', mobile1);
      formData.append('mobile2', mobile2);
      formData.append('email', email);
      formData.append('co_representative', co_representative);
      formData.append('company_email', company_email);
      formData.append('ipan_no', ipan_no);
      formData.append('bank_acc', bank_acc);
      formData.append('bank', bank);
      formData.append('branch', branch);
      formData.append('customer_classification', customer_classification);
      formData.append('first_relation', first_relation);
      formData.append('last_relation', last_relation);
      formData.append('remarks', remarks);

      console.log(formData)
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'customer/update', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getCustomers(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'customer/list', {headers : headers}).pipe(
      map(this.extractData));
  }


  addVendor(
    name_en: string,
    name_ar: string,
    type: string,
    phone: string,
    mobile: string,
    fax: string,
    web: string,
    country: string,
    province: string,
    city: string,
    address: string,
    mobile1: string,
    mobile2: string,
    email: string,
    co_representative: string,
    company_email: string,
    ipan_no: string,
    bank_acc: string,
    bank: string,
    branch: string,
    vendor_classification: string,
    first_relation: string,
    last_relation: string,
    remarks: string
    ): Observable<any> {
      let formData: FormData = new FormData();

      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('type', type);
      formData.append('phone', phone);
      formData.append('mobile', mobile);
      formData.append('fax', fax);
      formData.append('web', web);
      formData.append('country', country);
      formData.append('province', province);
      formData.append('city', city);
      formData.append('address', address);
      formData.append('mobile1', mobile1);
      formData.append('mobile2', mobile2);
      formData.append('email', email);
      formData.append('co_representative', co_representative);
      formData.append('company_email', company_email);
      formData.append('ipan_no', ipan_no);
      formData.append('bank_acc', bank_acc);
      formData.append('bank', bank);
      formData.append('branch', branch);
      formData.append('vendor_classification', vendor_classification);
      formData.append('first_relation', first_relation);
      formData.append('last_relation', last_relation);
      formData.append('remarks', remarks);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'vendor', formData, {headers: headers}).pipe(
      map(this.extractData));
  }


  editVendor(
    id: string,
    name_en: string,
    name_ar: string,
    type: string,
    phone: string,
    mobile: string,
    fax: string,
    web: string,
    country: string,
    province: string,
    city: string,
    address: string,
    mobile1: string,
    mobile2: string,
    email: string,
    co_representative: string,
    company_email: string,
    ipan_no: string,
    bank_acc: string,
    bank: string,
    branch: string,
    vendor_classification: string,
    first_relation: string,
    last_relation: string,
    remarks: string
    ): Observable<any> {
      let formData: FormData = new FormData();

      formData.append('id', id);
      formData.append('name_en', name_en);
      formData.append('name_ar', name_ar);
      formData.append('type', type);
      formData.append('phone', phone);
      formData.append('mobile', mobile);
      formData.append('fax', fax);
      formData.append('web', web);
      formData.append('country', country);
      formData.append('province', province);
      formData.append('city', city);
      formData.append('address', address);
      formData.append('mobile1', mobile1);
      formData.append('mobile2', mobile2);
      formData.append('email', email);
      formData.append('co_representative', co_representative);
      formData.append('company_email', company_email);
      formData.append('ipan_no', ipan_no);
      formData.append('bank_acc', bank_acc);
      formData.append('bank', bank);
      formData.append('branch', branch);
      formData.append('vendor_classification', vendor_classification);
      formData.append('first_relation', first_relation);
      formData.append('last_relation', last_relation);
      formData.append('remarks', remarks);

      console.log(formData)
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'vendor/update', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getVendors(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'vendor/list', {headers : headers}).pipe(
      map(this.extractData));
  }


  addDepartment(name: string, company_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('company_id', company_id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'department', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  deleteDepartment(id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'department/delete', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  editDepartment(id: string, name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'department/edit', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getDepartments(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'department/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addEngineer(name: string, company_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('company_id', company_id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'engineer', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  deleteEngineer(id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'engineer/delete', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  editEngineer(id: string, name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'engineer/edit', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getEngineers(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'engineer/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addConsultant(name: string, company_id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('company_id', company_id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'consultant', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  deleteConsultant(id: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'consultant/delete', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  editConsultant(id: string, name: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);
      formData.append('name', name);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'consultant/edit', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getConsultants(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'consultant/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addProject(
    name: string,
    start_date: string,
    end_date: string,
    location: string,
    engineer_id: string,
    owner_id: string,
    consultant_id: string,
    land_aria: string,
    status: string,
    stage: string,
    remarks: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name', name);
      formData.append('start_date', start_date);
      formData.append('end_date', end_date);
      formData.append('location', location);
      formData.append('engineer_id', engineer_id);
      formData.append('owner_id', owner_id);
      formData.append('consultant_id', consultant_id);
      formData.append('land_aria', land_aria);
      formData.append('status', status);
      formData.append('stage', stage);
      formData.append('remarks', remarks);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'project', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  editProject(
    id: string,
    name: string,
    start_date: string,
    end_date: string,
    location: string,
    engineer_id: string,
    owner_id: string,
    consultant_id: string,
    land_aria: string,
    status: string,
    stage: string,
    remarks: string): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('id', id);
      formData.append('name', name);
      formData.append('start_date', start_date);
      formData.append('end_date', end_date);
      formData.append('location', location);
      formData.append('engineer_id', engineer_id);
      formData.append('owner_id', owner_id);
      formData.append('consultant_id', consultant_id);
      formData.append('land_aria', land_aria);
      formData.append('status', status);
      formData.append('stage', stage);
      formData.append('remarks', remarks);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'project/edit', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getProjects(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'project/list', {headers : headers}).pipe(
      map(this.extractData));
  }

  addEmployee(
    name_ar : string,
    name_en : string,
    university : string,
    mobile_no : string,
    job_type : string,
    gender : string,
    dob : string,
    job : string,
    email : string,
    nationality : string,
    degree : string,
    country : string,
    province : string,
    city : string,
    aria : string,
    house_no : string,
    mobile_no_1 : string,
    mobile_no_2 : string,
    office_email : string,
    id_no : string,
    exp_date : string,
    passport_no : string,
    passport_exp_date : string,
    insurance_no : string,
    insurance_exp_date : string,
    ipan_no : string,
    acc_no : string,
    bank : string,
    branch : string,
    basic_salary : string,
    total_salary : string,
    start_date : string,
    end_date : string,
    job_status : string,
    management : string,
    job_id : string,
    company_id : string
    ): Observable<any> {
      let formData: FormData = new FormData();
      formData.append('name_ar', name_ar);
      formData.append('name_en', name_en);
      formData.append('university', university);
      formData.append('mobile_no', mobile_no);
      formData.append('job_type', job_type);
      formData.append('gender', gender);
      formData.append('dob', dob);
      formData.append('job', job);
      formData.append('email', email);
      formData.append('nationality', nationality);
      formData.append('degree', degree);
      formData.append('country', country);
      formData.append('province', province);
      formData.append('city', city);
      formData.append('aria', aria);
      formData.append('house_no', house_no);
      formData.append('mobile_no_1', mobile_no_1);
      formData.append('mobile_no_2', mobile_no_2);
      formData.append('office_email', office_email);
      formData.append('id_no', id_no);
      formData.append('exp_date', exp_date);
      formData.append('passport_no', passport_no);
      formData.append('passport_exp_date', passport_exp_date);
      formData.append('insurance_no', insurance_no);
      formData.append('insurance_exp_date', insurance_exp_date);
      formData.append('ipan_no', ipan_no);
      formData.append('acc_no', acc_no);
      formData.append('bank', bank);
      formData.append('branch', branch);
      formData.append('basic_salary', basic_salary);
      formData.append('total_salary', total_salary);
      formData.append('start_date', start_date);
      formData.append('end_date', end_date);
      formData.append('job_status', job_status);
      formData.append('management', management);
      formData.append('job_id', job_id);
      formData.append('company_id', company_id);

      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.post(environment.endpoint + 'employee', formData, {headers: headers}).pipe(
      map(this.extractData));
  }

  getEmployees(): Observable<any> {
      const headers = new HttpHeaders()
      .set("token", localStorage.getItem("token"));

      return this.http.get(environment.endpoint + 'employee/list', {headers : headers}).pipe(
      map(this.extractData));
  }
}
