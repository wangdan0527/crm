// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endpoint: 'http://192.168.1.101:8888/crm_backend/crm_backend/v1/public/',
  firebaseConfig : {
    apiKey: "AIzaSyCSphLwbnWrD1Vs-zlBW3FuYkF-ih6_vqA",
    authDomain: "hudhud-crm.firebaseapp.com",
    databaseURL: "https://hudhud-crm.firebaseio.com",
    projectId: "hudhud-crm",
    storageBucket: "hudhud-crm.appspot.com",
    messagingSenderId: "533711672857",
    appId: "1:533711672857:web:ec8f8befe86846c28b3c88",
    measurementId: "G-WT42DMZQLE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
