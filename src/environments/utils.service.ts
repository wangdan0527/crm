import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  	constructor() { }

  	public static calculateArea() {
        var result = '';
	   	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	   	var charactersLength = characters.length;
	   	for ( var i = 0; i < 10; i++ ) {
	    	result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   	}
	   	return result;
    }
}
